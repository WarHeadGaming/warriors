package com.warhead.warriors.listeners;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.components.RPGComponent;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerLeaveEvent;
import org.spout.api.event.player.PlayerLoginEvent;

/**
 *
 * @author Somners
 */
public class WarriorsListener implements Listener{
    
    private MySQLManager mysql;
    
    public WarriorsListener(){
        mysql = Warriors.getDatabase();
    }
    
    @EventHandler(order = Order.LATEST)
    public void onLogin(PlayerLoginEvent event){
//        Warriors.getLog().info("|||TEST|||   login");
        Player player = event.getPlayer();
        if(WarriorsManager.doesPlayerHaveCharacter(player.getName())){
            player.add(RPGComponent.class).refreshComponent();
        } else {
            
        }
    }
    
    @EventHandler
    public void onPlayerLeave(PlayerLeaveEvent event){
        Player player = event.getPlayer();
        if(player.get(RPGComponent.class) != null){
            player.get(RPGComponent.class).refreshComponent();
        }
    } 

}
