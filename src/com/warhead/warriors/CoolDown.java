/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author somners
 */

public class CoolDown {
        private static HashMap<String, List<String>> map = new HashMap<String, List<String>>();


        public CoolDown() {
            
        }

        /**
         * add a new cool-down for a skill
         * @param playerName name of the player
         * @param skillName name of the skill
         * @param time time in seconds of cool-down
         * @return true = cool-down was added<br>false = cool-down already existed 
         */
        public boolean addCooldown(String playerName, String skillName, int time){
            if(!map.containsKey(playerName)){
                map.put(playerName, new ArrayList());
                map.get(playerName).add(skillName);
                Timer t = new Timer();
                t.schedule(new Task(playerName, skillName), time*1000);
                return true;
            }
            else{
                if(map.get(playerName).contains(skillName)){
                    return false;
                }
                else{
                    map.get(playerName).add(skillName);
                    Timer t = new Timer();
                    t.schedule(new Task(playerName, skillName), time*1000);
                    return true;
                }
            }
        }
        
        /**
         * add a new cool-down for a skill
         * @param playerName name of the player
         * @param skillName name of the skill
         * @return true = skill is in cool-down <br>false = skill is not in cool-down
         */
        public boolean isInCooldown(String playerName, String skillName){
            if(map.containsKey(playerName) && map.get(playerName).contains(skillName)){
                return true;
            }
            return false;
        }

        private class Task extends TimerTask {
            private String playerName;
            private String skillName;
            
            public Task(String playerName, String skillName){
                this.playerName = playerName;
                this.skillName = skillName;
            }
            
            @Override
            public void run() {
                if(map.get(playerName).contains(skillName)){
                    map.get(playerName).remove(skillName);
                    if(map.get(playerName).isEmpty()){
                        map.remove(playerName);
                    }
                }
            }
        }
}

     