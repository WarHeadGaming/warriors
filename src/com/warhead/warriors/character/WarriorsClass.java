/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.character;

import java.util.ArrayList;

/**
 *
 * @author somners
 */
public class WarriorsClass {
    String className;
    ArrayList<Item> items = new ArrayList<Item>();
    ArrayList<Skill> skills = new ArrayList<Skill>();
    ArrayList<String> bannedRaces = new ArrayList<String>();
    Prerequisites prerequisites;
}
