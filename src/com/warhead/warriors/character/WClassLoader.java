/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.character;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.warcharacter.WarClass;
import com.warhead.warriors.api.warcharacter.WarClassLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class WClassLoader implements WarClassLoader{

    private static HashMap<String, WarClass> characters = new HashMap<String, WarClass>();
    private static File dir = new File("plugins/Warriors/Classes/");

    public WClassLoader(){
        if(!dir.exists()){
            dir.mkdirs();
            if(Spout.debugMode())
                Warriors.getLog().info(" Class directory created.");
        }
    }

    @Override
    public boolean isWarCharacterLoaded(String characterName){
        return characters.containsKey(characterName);
    }

    @Override
    public List<String> getWarCharacterNames(){
        List<String> warCharacters = new ArrayList();
        for(String s: characters.keySet()){
            warCharacters.add(s);
        }
        return warCharacters;
    }

    @Override
    public List<WarClass> getWarCharacters(){
        List<WarClass> warCharacters = new ArrayList();
        WarClass[] characterArray = (WarClass[])this.characters.values().toArray();
        for(int i = 0 ; characterArray.length < i ; i++){
            warCharacters.add(characterArray[i]);
        }
        return warCharacters;
    }

    @Override
    public WarClass getWarCharacter(String characterName) {
        return characters.get(characterName);
    }

    @Override
    public void loadWarCharacter(String name) {
        File xml = new File(dir, name + ".xml");
        File yml = new File(dir, name + ".yml");
        WarClass wc = null;
        if(xml.exists()){
            wc = new XMLClass(name);
        }
        else if(yml.exists()){
            
        }
        else{
            Warriors.getLog().severe(" Class'" + name + "' cannot be found. Does it exist?");
            return;
        }
        if(characters.containsKey(wc.getName())){
            Warriors.getLog().severe(" Class'" + wc.getName() + "' is already loaded, why do you have 2 files for it?");
            return;
        }
        characters.put(wc.getName(), wc);
        Warriors.getLog().info(" Character '" + wc.getName() + "' has been loaded!");

    }

    @Override
    public void loadAllWarCharacters() {
        String[] list = dir.list();
       for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".xml")){
                loadWarCharacter(list[i].replace(".xml", ""));
            }
            else if(list[i].contains(".yml")){
                loadWarCharacter(list[i].replace(".yml", ""));
            }
        }
    }

    @Override
    public void unloadWarCharacter(String name) {
        if(characters.containsKey(name)){
            characters.remove(name);
            Warriors.getLog().info(" Character '" + name + "' has been unloaded!");
            return;
        }
        Warriors.getLog().severe(" Character '" + name + "' cannot be unloaded beacause it isn't loaded.");
    }

    @Override
    public File getWarCharacterDir() {
        return dir;
    }
}
