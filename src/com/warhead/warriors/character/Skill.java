/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.character;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somners
 */
public class Skill {
    String name;
    Integer cooldown;
    Integer level;
    Integer manaCost;
    Integer staminaCost;
    List<String> achievements = new ArrayList<String>();
}
