/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.character;

import com.thoughtworks.xstream.XStream;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.achievement.Achievement;
import com.warhead.warriors.api.warcharacter.WarClass;
import com.warhead.warriors.api.warcharacter.WarClassFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class XMLClass implements WarClass {

    WarriorsClass warriorsClass;
    XStream stream;
    
    public XMLClass(String name){
        InputStream in = null;
        try {
            File file = new File("plugins/Warriors/Classes/"+name+".xml");
            in = file.toURI().toURL().openStream();
            stream = new XStream();
            stream.alias("entry", String.class);
            stream.alias("class", WarriorsClass.class);
            stream.alias("item", Item.class);
            stream.alias("skill", Skill.class);
            stream.alias("class", WarriorsClass.class);
            warriorsClass = (WarriorsClass) stream.fromXML(in);
        } catch (IOException ex) {
            Warriors.getLog().info("Error Reading Class file: " + ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Warriors.getLog().info("Error Reading Class file: " + ex);
            }
        }
    }

    @Override
    public String getName() {
        try{
            return warriorsClass.className;
        }
        catch(NullPointerException npe){
            Warriors.getLog().log(Level.SEVERE,"[Warriors] A Class lacks a name. Go check your Configs!");
            return "";
        }
    }

    @Override
    public boolean canClassUseSkill(String skillName) {
        if(warriorsClass.skills != null){
            if(warriorsClass.skills != null){
                for(Skill s: warriorsClass.skills){
                    if(skillName.equalsIgnoreCase(s.name)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int getItemBaseDamage(String tool) {
        if(warriorsClass.items != null){
            for(Item t : warriorsClass.items){
                if(tool.equalsIgnoreCase(t.name)){
                    return t.baseDamage == null ? 1 : t.baseDamage.intValue();
                }
            }
        }
        return 1;
    }

    @Override
    public int getItemMaxDamage(String tool) {
        if(warriorsClass.items != null){
            for(Item t : warriorsClass.items){
                if(tool.equalsIgnoreCase(t.name)){
                    return t.maxDamage == null ? 1 : t.baseDamage.intValue();
                }
            }
        }
        return 1;
    }

    @Override
    public int getItemDamageModifier(String tool) {
        if(warriorsClass.items != null){
            for(Item t : warriorsClass.items){
                if(tool.equalsIgnoreCase(t.name)){
                    return t.skillPointDamageModifier == null ? 1 : t.skillPointDamageModifier.intValue();
                }
            }
        }
        return 1;
    }

    @Override
    public boolean canUseRace(String raceName) {
        if(warriorsClass.bannedRaces == null){
            return true;
        }
        return warriorsClass.bannedRaces == null ? true : !warriorsClass.bannedRaces.contains(raceName);
    }

    @Override
    public WarClassFile getWarClassFile() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillManaCost(String skill) {
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                if(skill.equalsIgnoreCase(s.name)){
                    return s.manaCost == null ? 0 : s.manaCost.intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public int getSkillStaminaCost(String skill) {
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                if(skill.equalsIgnoreCase(s.name)){
                    return s.manaCost == null ? 0 : s.staminaCost.intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean canClassUseTool(String toolName) {
        if(warriorsClass.items != null){
            for(Item t: warriorsClass.items){
                if(toolName.equalsIgnoreCase(t.name)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> getSkillNames() {
        List<String> toRet = new ArrayList<String>();
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                toRet.add(s.name);
            }
        }
        return toRet;
    }

    @Override
    public List<Achievement> getSkillAchievements(String skill) {
        List<Achievement> toRet = new ArrayList<Achievement>();
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                if(skill.equalsIgnoreCase(s.name)){
                    for(String ss : s.achievements){
                        toRet.add(WarriorsManager.getAchievementLoader().getAchievement(ss));
                    }
                }
            }
        }
        return toRet;
    }
    
    @Override
    public List<String> getToolNames(){
        List<String> toRet = new ArrayList<String>();
        for(Item t : warriorsClass.items){
            toRet.add(t.name);
        }
        return toRet;
    }

    @Override
    public List<Achievement> getItemAchievements(String tool) {
        List<Achievement> toRet = new ArrayList<Achievement>();
        if(warriorsClass.items != null){
            for(Item t : warriorsClass.items){
                if(tool.equalsIgnoreCase(t.name)){
                    for(String tt : t.achievements){
                        toRet.add(WarriorsManager.getAchievementLoader().getAchievement(tt));
                    }
                }
            }
        }
        return toRet;
    }

    @Override
    public List<String> getBannedRaces() {
        return warriorsClass.bannedRaces == null ? new ArrayList<String>() : warriorsClass.bannedRaces;
    }

    @Override
    public int getSkillLevel(String skill) {
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                if(s.name.equalsIgnoreCase(skill)){
                    return s.level == null ? 0 : s.level.intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public int getItemLevel(String tool) {
        if(warriorsClass.items != null){
            for(Item t : warriorsClass.items){
                if(t.name.equalsIgnoreCase(tool)){
                    return t.level == null ? 0 : t.level.intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public int getSkillCooldown(String skill) {
        if(warriorsClass.skills != null){
            for(Skill s : warriorsClass.skills){
                if(s.name.equalsIgnoreCase(skill)){
                    return s.cooldown == null ? 0 : s.cooldown.intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean isPrimaryClass() {
	return warriorsClass.prerequisites == null;
    }

    @Override
    public boolean isSecondaryClass() {
	return warriorsClass.prerequisites != null;
    }

    @Override
    public String getPrereqClassName() {
	if(isSecondaryClass()){
	    return warriorsClass.prerequisites.className;
	}
	return null;
    }

    @Override
    public int getPrereqClassLevel() {
	if(isSecondaryClass() && warriorsClass.prerequisites.level != null){
	    return warriorsClass.prerequisites.level;
	}
	return 0;
    }
}
