/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.character;

import com.warhead.warriors.api.warcharacter.WarClassFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.ConfigurationNode;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author somners
 */
public class WClassFile extends ConfigurationHolderConfiguration implements WarClassFile {

    private YamlConfiguration config;
    private ConfigurationHolder className = new ConfigurationHolder(config, (Object)"Default Name", "Core", "Class Name");
    private ConfigurationHolder skills = new ConfigurationHolder(config, (Object)new ArrayList(), "Core", "Skills");
    private ConfigurationHolder oreXP = new ConfigurationHolder(config, (Object)"", "Core", "Xp Rewards", "Ores");
    private ConfigurationHolder mobXP = new ConfigurationHolder(config, (Object)"", "Core", "Xp Rewards", "Mobs");
    private ConfigurationHolder weapons = new ConfigurationHolder(config, (Object)"", "Core", "Weapon");
    private ConfigurationHolder tools = new ConfigurationHolder(config, (Object)"", "Core", "Tool");
    private ConfigurationHolder bannedRaces = new ConfigurationHolder(config, (Object)"", "Core", "Banned Races");

    public WClassFile(Configuration config) throws ConfigurationException {
        super(config);
        this.config = (YamlConfiguration) config;
        load();
    }


    @Override
    public String getNameEntry() {
        return className.getString();
    }

    @Override
    public HashMap<String, Integer> getSkillsEntry() {
        Map<String, ConfigurationNode> map = config.getNode("Core.Skills").getChildren();
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getInt());
        }
        return returnMap;
    }

    @Override
    public HashMap<String, Integer> getXpOreEntry() {
        Map<String, ConfigurationNode> map = oreXP.getChildren();
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getInt());
        }
        return returnMap;
    }

    @Override
    public HashMap<String, Integer> getXpMobEntry() {
        Map<String, ConfigurationNode> map = mobXP.getChildren();
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getInt());
        }
        return returnMap;
    }

    @Override
    public List getCustomListEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCustomStringEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCustomIntEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getCustomBooleanEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getCustomDoubleEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean doesEntryExist(String node, Object defaultValue) {
        ConfigurationNode testNode = config.getNode(node);
        if (testNode.getValue() == null) {
            try {
                config.createConfigurationNode(node.split("\\."), defaultValue);
                config.save();
            } catch (ConfigurationException ex) {
                Logger.getLogger(WClassFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        return true;
    }

    @Override
    public HashMap<String, Integer> getWeaponLevels() {
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Map<String, ConfigurationNode> map = weapons.getChildren();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getNode("Skill Points Needed").getInt());
        }
        return returnMap;
    }

    @Override
    public HashMap<String, Integer> getWeaponBaseDamage() {
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Map<String, ConfigurationNode> map = weapons.getChildren();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getNode("Base Damage").getInt());
        }
        return returnMap;
    }

    @Override
    public HashMap<String, Integer> getWeaponMaxDamage() {
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Map<String, ConfigurationNode> map = weapons.getChildren();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getNode("Max Damage").getInt());
        }
        return returnMap;
    }

    @Override
    public HashMap<String, Integer> getToolLevels() {
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Map<String, ConfigurationNode> map = tools.getChildren();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getNode("Skill Points Needed").getInt());
        }
        return returnMap;
    }

    @Override
    public int getSwordDamageDiff() {
        Map<String, ConfigurationNode> map = config.getNode("Core.Weapon").getChildren();
        Iterator<String> keys = map.keySet().iterator();
        int toReturn = 0;
        while (keys.hasNext()) {
            String key = keys.next();
            int temp = !key.equalsIgnoreCase("Bow") ? map.get(key).getNode("Max Damage").getInt() - map.get(key).getNode("Base Damage").getInt() : 0;
            if (temp > toReturn) {
                toReturn = temp;
            }
        }
        return toReturn;
    }

    @Override
    public int getBowDamageDiff() {
        return config.getNode("Core.Weapon.Bow.Max Damage").getInt() - config.getNode("Core.Weapon.Bow.Base Damage").getInt();
    }

    @Override
    public List<String> getBannedRaces() {
        return bannedRaces.getStringList();
    }

    public boolean doesListExist(String node, List defaultValue) {
        ConfigurationNode testNode = config.getNode(node);
        if (testNode.getList().isEmpty()) {
            try {
                config.addChild(new ConfigurationNode(config, node.split("\\."), defaultValue));
                config.save();
            } catch (ConfigurationException ex) {
                Logger.getLogger(WClassFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        return true;
    }
    
    @Override
    public void save() throws ConfigurationException{
        super.save();
    }
    
    @Override
    public void load() throws ConfigurationException {
        super.load();
        super.save();
    }
}
