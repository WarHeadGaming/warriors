
package com.warhead.warriors.character;

import com.warhead.warriors.api.achievement.Achievement;
import com.warhead.warriors.api.warcharacter.WarClass;
import com.warhead.warriors.api.warcharacter.WarClassFile;
import java.util.List;
import org.spout.api.exception.ConfigurationException;

/**
 *
 * @author somners
 */
public class YamlClass implements WarClass {



    public YamlClass(WarClassFile config) throws ConfigurationException{
        
    }

    @Override
    public String getName() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canClassUseSkill(String skillName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getItemBaseDamage(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getItemMaxDamage(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getItemDamageModifier(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canUseRace(String raceName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public WarClassFile getWarClassFile() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillManaCost(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillStaminaCost(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canClassUseTool(String toolName) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getSkillNames() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Achievement> getSkillAchievements(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Achievement> getItemAchievements(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getBannedRaces() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getToolNames() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillLevel(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getItemLevel(String tool) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSkillCooldown(String skill) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isPrimaryClass() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isSecondaryClass() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPrereqClassName() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPrereqClassLevel() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
