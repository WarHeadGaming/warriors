package com.warhead.warriors.api.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.material.Material;

/**
 * Interface for Weapon Enumerations, this will allow us to dynamically use
 * weapons from any game.
 * 
 * @author Somners
 */
public abstract class ItemHandler {
    
    protected List<String> items = new ArrayList<String>();
    
    public ItemHandler(){
        this.loadItemList();
    }
    
    /**
     * Check if this item is restricted on a per class basis. returns true if it
     * is found in the Items.txt
     * @param itemName name of the item to check
     * @return whether or not it is managed
     */
    public abstract boolean isManagedItem(String itemName);
    
    /**
     * Check if this item is restricted on a per class basis. returns true if it
     * is found in the Items.txt
     * @param material Material of the item to check
     * @return whether or not it is managed
     */
    public abstract boolean isManagedItem(Material material);
    
    /**
     * get the Material.class instance of this item
     * @param itemName name to get material for
     * @return the material, null if it does not exist
     */
    public abstract Material getItemMaterial(String itemName);
    
    /**
     * get the name of the given material
     * @param material
     * @return the name
     */
    public abstract String getItemName(Material material);
    
    public void loadItemList() {
        File file = new File(new File("plugins/Warriors/"), "Items.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
                
                
                
                
            } catch (IOException ex) {
                Logger.getLogger(VanillaItemHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            
        }
        else{
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String item = reader.readLine();
                while(item != null){
                    if(Material.get(item) != null){
                        items.add(item);
                    }
                }
                if(reader != null){
                    reader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(VanillaItemHandler.class.getName()).log(Level.SEVERE, null, ex);
            } 
            
        }
    }
    
}
