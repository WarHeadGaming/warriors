package com.warhead.warriors.api.Util;

import org.spout.api.material.Material;

/**
 *
 * @author somners
 */
public class VanillaItemHandler extends ItemHandler{
    
    
    public VanillaItemHandler() {
        super();
        
    }

    @Override
    public boolean isManagedItem(String itemName) {
        return items.contains(itemName);
    }

    @Override
    public boolean isManagedItem(Material material) {
        return items.contains(material.getDisplayName());
    }

    @Override
    public Material getItemMaterial(String itemName) {
        return Material.get(itemName);
    }

    @Override
    public String getItemName(Material material) {
        return material.getDisplayName();
    }
    
}
