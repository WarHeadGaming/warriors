/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.api.warcharacter;

import com.warhead.warriors.api.achievement.Achievement;
import java.util.List;

/**
 *
 * @author somners
 */
public interface WarClass {

    /**
     * get the name of this WarCharacter
     * @return the name
     */
    public String getName();

    /**
     * check if a player can use this skill<br>
     * player must belong to this character class for it to work
     * @param player player you are checking for
     * @param skillName name of the skill you are checking
     * @return true = they can use the skill<br>
     * false = they cannot use the skill
     */
    public boolean canClassUseSkill(String skillName);

    /**
     * 
     * @param tool
     * @return 
     */
    public int getItemBaseDamage(String tool);
    
    public int getItemMaxDamage(String tool);
    
    public int getItemDamageModifier(String tool);

    public boolean canUseRace(String raceName);
    
    public WarClassFile getWarClassFile();
    
    public int getSkillManaCost(String skill);
    
    public int getSkillStaminaCost(String skill);
    
    public boolean canClassUseTool(String toolName);
    
    public List<String> getSkillNames();
    
    public List<Achievement> getSkillAchievements(String skill);
    
    public int getSkillLevel(String skill);
    
    public int getSkillCooldown(String skill);
    
    public List<Achievement> getItemAchievements(String tool);
    
    public int getItemLevel(String tool);

    public List<String> getBannedRaces();
    
    public List<String> getToolNames();
    
    public boolean isPrimaryClass();
    
    public boolean isSecondaryClass();
    
    public String getPrereqClassName();
    
    public int getPrereqClassLevel();
    
}
