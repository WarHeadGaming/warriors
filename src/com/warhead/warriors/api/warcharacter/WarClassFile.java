package com.warhead.warriors.api.warcharacter;

import java.util.HashMap;
import java.util.List;

/**
 * represents a yaml configuration file for a WarCharacter
 * @author somners
 */
public interface WarClassFile {

    /**
     * gets the name entry of the WarCharacter
     * @return name entry
     */
    public String getNameEntry();

    public HashMap<String, Integer> getWeaponLevels();

    public HashMap<String, Integer> getToolLevels();


    /**
     * gets a HashMap of skill name entries for the WarCharacter<br>
     * key= skillName<br>
     * value= level
     * @return skill entries
     */
    public HashMap<String, Integer> getSkillsEntry();

    /**
     * gets a HashMap of ore xp entries for the WarCharacter<br>
     * key= oreID<br>
     * value= xp
     * @return ore xp Entries
     */
    public HashMap<String, Integer> getXpOreEntry();

    /**
     *gets a HashMap of mob xp entries for the WarCharacter<br>
     * key= mobName<br>
     * value= xp
     * @return mob xp Entries
     */
    public HashMap<String, Integer> getXpMobEntry();

    /**
     * gets a custom value from this warriors configuration file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public List getCustomListEntry(String node);

    /**
     * gets a custom value from this warriors configuration file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public String getCustomStringEntry(String node);

    /**
     * gets a custom value from this warriors configuration file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public int getCustomIntEntry(String node);

    /**
     * gets a custom value from this warriors configuration file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public boolean getCustomBooleanEntry(String node);

    /**
     * gets a custom value from this warriors configuration file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public double getCustomDoubleEntry(String node);

    /**
     * checks if this entry exists and creates it if it doesn't with the
     * given default value
     * @param node the node you are checking for
     * @param defaultValue the default value if the node doesn't exist
     * @return
     */
    public boolean doesEntryExist(String node, Object defaultValue);

    public HashMap<String, Integer> getWeaponMaxDamage();

    public HashMap<String, Integer> getWeaponBaseDamage();

    public int getBowDamageDiff();

    public int getSwordDamageDiff();

    public List<String> getBannedRaces();
    
}
