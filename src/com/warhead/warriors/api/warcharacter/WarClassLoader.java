package com.warhead.warriors.api.warcharacter;

import java.io.File;
import java.util.List;

/**
 *
 * @author somners
 */
public interface WarClassLoader {

    /**
     * check if a WarCharacter is loaded
     * @param characterName name of the Character
     * @return true = it is loaded<br>
     * false = it is not loaded
     */
    public boolean isWarCharacterLoaded(String characterName);

    /**
     * get a WarCharacter class
     * @param characterName name of the Character
     * @return the WarCharacter
     */
    public WarClass getWarCharacter(String characterName);

    /**
     * get names of the WarCharacter classes
     * @return a List of WarCharacter class names
     */
    public List<String> getWarCharacterNames();

    /**
     * get all the WarCharacter classes
     * @return a List that contains all the WarCharacter classes
     */
    public List<WarClass> getWarCharacters();

    /**
     * load a WarCharacter class
     * @param name name of the character class to load
     */
    public void loadWarCharacter(String name);

    /**
     * load all WarCharacters present in the directory
     */
    public void loadAllWarCharacters();

    /**
     * unload a WarCharacter
     * @param name name of the character to unload
     */
    public void unloadWarCharacter(String name);

    /**
     * get the directory the WarCharacter configurations are located
     * @return a File of the directory
     */
    public File getWarCharacterDir();

}
