/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.races;

import java.util.HashMap;
import java.util.List;
import org.spout.api.util.config.ConfigurationHolder;

/**
 * represents a yaml configuration file for a WarCharacter
 * @author somners
 */
public interface RaceFile {

    /**
     * gets the name entry of the WarCharacter
     * @return name entry
     */
    public String getNameEntry();

    public int getBaseHealth();

    public int getMaxHealth();

    public int getBaseMana();

    public int getMaxMana();

    public int getBaseStamina();

    public int getMaxStamina();

    /**
     * gets a HashMap of skill name entries for the WarCharacter<br>
     * key= skillName<br>
     * value= level
     * @return skill entries
     */
    public HashMap<String, Integer> getSkillsEntry();

    /**
     * gets a custom value from this warriors config file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public List getCustomListEntry(String node);

    /**
     * gets a custom value from this warriors config file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public String getCustomStringEntry(String node);

    /**
     * gets a custom value from this warriors config file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public int getCustomIntEntry(String node);

    /**
     * gets a custom value from this warriors config file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public boolean getCustomBooleanEntry(String node);

    /**
     * gets a custom value from this warriors config file<br>
     * node example= "mySkillName.someKey"
     * @param node the node you wish to get thee value for
     * @return your custom value
     */
    public double getCustomDoubleEntry(String node);

    /**
     * checks if this entry exists and creates it if it doesn't with the
     * given default value
     * @param node the node you are checking for
     * @param defaultValue the default value if the node doesn't exist
     * @return
     */
    public boolean doesEntryExist(ConfigurationHolder holder,String node, Object defaultValue);

    public int getSPHealthModifier();

    public int getSPManaModifier();

    public int getSPStaminaModifier();
    
    public HashMap<Integer, Integer> getWeaponSPDamageMod();

    public int getBowDamageDiff();

    public int getSwordDamageDiff();
}
