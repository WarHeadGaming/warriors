/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.races;

import java.io.File;
import java.util.List;

/**
 *
 * @author somners
 */
public interface RaceLoader {

    /**
     * check if a Race is loaded
     * @param raceName name of the race
     * @return true = it is loaded<br>
     * false = it is not loaded
     */
    public boolean isRaceLoaded(String raceName);

    /**
     * get a Race class
     * @param raceName name of the race
     * @return the Race
     */
    public Race getRace(String characterName);

    /**
     * get names of the Race classes
     * @return a List of Race class names
     */
    public List<String> getRaceNames();

    /**
     * get all the Race classes
     * @return a List that contains all the Race classes
     */
    public List<Race> getRaces();

    /**
     * load a Race class
     * @param name name of the race class to load
     */
    public void loadRace(String name);

    /**
     * load all WarCharacters present in the directory
     */
    public void loadAllRaces();

    /**
     * unload a Race
     * @param name name of the race to unload
     */
    public void unloadRace(String name);

    /**
     * get the directory the Race configurations are located
     * @return a File of the directory
     */
    public File getRaceDir();

}
