/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.api.races;

/**
 *
 * @author somners
 */
public interface Race {

    /**
     * get the name of this Race
     * @return the name
     */
    public String getName();

    public int getBaseMana();

    public int getMaxMana();

    public int getBaseStamina();

    public int getMaxStamina();

    public int getBaseHealth();

    public int getMaxHealth();

    public int getSPHealthModifier();

    public int getSPManaModifier();

    public int getSPStaminaModifier();
    
    public int getManaRegenInterval();
    
    public float getManaRegenRate();
    
    public int getManaSPRegenModifier();
    
    public int getStaminaRegenInterval();
    
    public float getStaminaRegenRate();
    
    public int getStaminaSPRegenModifier();
    
    public int getHealthRegenInterval();
    
    public float getHealthRegenRate();
    
    public int getHealthSPRegenModifier();

    public boolean hasMana();
    
    public boolean hasStamina();
    
}
