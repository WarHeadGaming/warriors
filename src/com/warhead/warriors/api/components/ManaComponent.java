package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.WarriorsConfig;
import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.api.trigger.PlayerManaChangeTrigger;
import com.warhead.warriors.api.trigger.TriggerManager;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class ManaComponent extends EntityComponent implements WarComponent{

    private int mana = 0;
    private int manaBase = 0;
    private int manaMax = 0;
    private int manaSP = 0;
    private int manaFull = 0;
    private int manaRegenSP = 0;
    private int manaModifier = 0;
    private MySQLManager mysql;
    private boolean isLoaded = false;
    private int lastUpdate = 0;

    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }

    @Override
    public void refreshComponent(){
        writeToDatabase();
        isLoaded = true;
        readFromDatabase();
        manaModifier = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getSPManaModifier();
        manaMax = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getMaxMana();
        manaBase = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getBaseMana();
        int tempManaFull = ((Player)super.getOwner()).get(RPGComponent.class).hasRace() ? ( manaBase + (manaModifier * manaSP ) ) : ( manaBase + (manaModifier * manaSP) ) ;
        manaFull = tempManaFull <= manaMax ? tempManaFull : manaMax;
    }
    
    
    @Override
    public void onTick(float dt){
        ++lastUpdate;
        Race race = ((Player)super.getOwner()).get(RPGComponent.class).getRace();
        if(race.getManaRegenInterval() <= lastUpdate/20 && getManaPoints() <= getFullManaValue()){
            lastUpdate = 0;
            float percent = (race.getManaSPRegenModifier() * manaRegenSP) + race.getManaRegenRate();
            addMana((int)percent*manaFull >= 1 ? (int)percent*manaFull : 1, ManaChangeCause.REGEN);
            ((Player)super.getOwner()).sendMessage(WarriorsConfig.getServerHeader().append(" Mana: " +getMana() + "/" + getFullManaValue()));
        }
    }

    /**
     * gets how much mana this player has when its full.
     * @return 
     */
    public int getFullManaValue(){
        return ( manaBase + (manaModifier*manaSP) ) <= manaMax ? ( manaBase + (manaModifier*manaSP) ) : manaMax;
    }
    public int getManaPoints() {
        return ((Player)super.getOwner()).get(SkillPointComponent.class).getSkillPoints();
    }


    public void addManaPoint() {
        manaSP++;
        refreshComponent();
    }


    public void removeManaPoint() {
        manaSP--;
        refreshComponent();
    }


    public void setManaPoint(int toSet) {
        manaSP = toSet;
        refreshComponent();
    }
    
    public void addManaPoints(int toAdd){
        manaSP = manaSP + toAdd;
    }

    public int getMana() {
        return mana;
    }


    public int getMaxMana() {
        return manaMax;
    }


    public void setMana(int toSet, ManaChangeCause source) {
        if(toSet > getFullManaValue() || toSet < 0) {return;}
        PlayerManaChangeTrigger pmct = new PlayerManaChangeTrigger(((Player)super.getOwner()), mana, toSet, source);
        if(Spout.debugMode())
            Warriors.getLog().info("Set mana of Player " + ((Player)super.getOwner()).getName() + " to " + toSet);
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        if(toSet > getFullManaValue()){mana = getFullManaValue();}
        else if(toSet < 0){mana = 0;}
        else{mana = toSet;}
    }


    public void setMaxMana(int toSet) {
        manaMax = toSet;
    }


    public int getSPManaModifier(){
        return manaModifier;
    }


    public void addMana(int toAdd, ManaChangeCause source) {
        PlayerManaChangeTrigger pmct = new PlayerManaChangeTrigger(((Player)super.getOwner()), mana, mana + toAdd, source);
        if(Spout.debugMode())
            Warriors.getLog().info("Added " + toAdd + " mana to Player " + ((Player)super.getOwner()).getName());
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        mana = (mana + toAdd) >= getFullManaValue() ? getFullManaValue() : mana + toAdd;
    }


    public void removeMana(int toRemove, ManaChangeCause source) {
        PlayerManaChangeTrigger pmct = new PlayerManaChangeTrigger(((Player)super.getOwner()), mana, mana - toRemove, source);
        if(Spout.debugMode())
            Warriors.getLog().info("Removed " + toRemove + " mana to Player " + ((Player)super.getOwner()).getName());
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        mana = (mana - toRemove) <= 0 ? 0 : mana - toRemove;
    }

        
    public void addManaRegenPoint() {
        manaRegenSP++;
        refreshComponent();
    }


    public void removeManaRegenPoint() {
        manaRegenSP--;
        refreshComponent();
    }

    public void addManaRegenPoints(int toAdd){
        manaRegenSP = manaRegenSP + toAdd;
        refreshComponent();
    }

    public void removeManaRegenPoints(int toRemove){
        manaRegenSP = manaRegenSP - toRemove < 0 ? 0 : manaRegenSP - toRemove;
        refreshComponent();
    }

    public boolean isManaMaxedOut(){
        if(manaMax < (manaBase + (manaSP*manaModifier))){
            return false;
        }
        return true;
    }

    @Override
    public void readFromDatabase() {
        mana = mysql.getIntValue("SELECT `mana` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "mana");
        manaSP = mysql.getIntValue("SELECT `mana` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "mana");
        manaRegenSP = mysql.getIntValue("SELECT `mana_regen` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "mana_regen");
    }

    @Override
    public void writeToDatabase() {
        if (isLoaded){
            mysql.update("UPDATE `warriors_skill_points` SET `mana` = '"+ manaSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            mysql.update("UPDATE `warriors_players` SET `mana` = '"+ mana +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            mysql.update("UPDATE `warriors_skill_points` SET `mana_regen` = '"+ manaRegenSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
        }
    }
}