package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.achievement.Achievement;
import com.warhead.warriors.api.achievement.AchievementLoader;
import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.api.skill.Skill;
import com.warhead.warriors.api.warcharacter.WarClass;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 * This class represents the main component for the Warriors Plugin. you can get
 * all our other components from this class.
 * 
 * @author somners
 */
public class RPGComponent extends EntityComponent implements WarComponent{

    private MySQLManager mysql;
    private boolean isLoaded = false;
    private WarClass[] characters = new WarClass[2];
    private WarriorsHealth health = null;
    private Race race = null;
    private String characterName = "";
    private String character2Name = "";
    private String raceName = "";

    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }
    
    @Override
    public void onDetached(){
        Player player = (Player)super.getOwner();
        player.detach(ManaComponent.class);
        player.detach(StaminaComponent.class);
        player.detach(SkillPointComponent.class);
        player.detach(ToolComponent.class);
        player.detach(XpComponent.class);
    }

    @Override
    public void readFromDatabase() {
        characterName = mysql.getStringValue("SELECT `character_name` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "character_name");
        character2Name = mysql.getStringValue("SELECT `character_2_name` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "character_2_name");
        raceName = mysql.getStringValue("SELECT `race_name` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "race_name");
    }

    @Override
    public void writeToDatabase() {
        if(isLoaded){
            if(characters[0] != null) mysql.update("UPDATE `warriors_players` SET `character_name` = '"+ characters[0].getName() +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            if(characters[1] != null) mysql.update("UPDATE `warriors_players` SET `character_2_name` = '"+ characters[1].getName() +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            mysql.update("UPDATE `warriors_players` SET `race_name` = '"+ race.getName() +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
        }
    }

    /**
     * Get our wrapper for the health component of whatever game this is.
     * @return Health.class
     */
    public WarriorsHealth getHealth(){
        return health;
    }
    
    /**
     * Get this players ManaComponent
     * @return ManaComponent.class
     */
    public ManaComponent getManaComponent(){
        return ((Player)super.getOwner()).get(ManaComponent.class);
    }
    
    /**
     * Get this players StaminaComponent
     * @return StaminaComponent.class
     */
    public StaminaComponent getStaminaComponent(){
        return ((Player)super.getOwner()).get(StaminaComponent.class);
    }

    /**
     * SkillPointComponent this class manages unused skill points
     * @return SkillPointComponent.class
     */
    public SkillPointComponent getSkillPointComponent(){
        return ((Player)super.getOwner()).get(SkillPointComponent.class);
    }
    
    /**
     * Get this players ToolComponent
     * @return ToolComponent.class
     */
    public ToolComponent getToolComponent(){
        return ((Player)super.getOwner()).get(ToolComponent.class);
    }

    /**
     * Gets the players name
     * @return the players name
     */
    public String getPlayerName() {
        return ((Player)super.getOwner()).getName();
    }
    
    /**
     * return whether or not this player has a WarCharacer; should be true...
     * @return whether or not is has a WarCharacter
     */
    public boolean hasPrimaryClass(){
        return characters[0] == null ? false : true;
    }
    
    /**
     * return whether or not this player has a secondary WarCharacer;
     * @return whether or not is has a WarCharacter
     */
    public boolean hasSecondaryClass(){
        return characters[1] == null ? false : true;
    }

    /**
     * Gets the name of this players WarCharacter class
     * @return the name
     */
    public String getPrimaryClassName() {
        return this.characters[0].getName();
    }

    /**
     * Gets the name of this players Secondary WarCharacter class.
     * @return the name, Null if they don't have one.
     */
    public String getSecondaryClassName() {
        return this.characters[0].getName();
    }
    
    /**
     * set this players WarCharacter
     * @param characterName WarCharacter to set
     */
    public void setPrimaryClass(String characterName){
        if(!WarriorsManager.getWarCharacterLoader().isWarCharacterLoaded(characterName)){
            Spout.getLogger().log(Level.WARNING, "[Warriors] someone attempted to set " +((Player)super.getOwner()).getName()+ " to race " + characterName + " which doesn't exist.");
        }
        this.characterName = characterName;
        characters[0] = WarriorsManager.getWarCharacterLoader().getWarCharacter(characterName);
        writeToDatabase();
    }
    
    /**
     * set this players secondary WarCharacter
     * @param characterName WarCharacter to set
     */
    public void setSecondaryClass(String characterName){
        if(!WarriorsManager.getWarCharacterLoader().isWarCharacterLoaded(characterName)){
            Spout.getLogger().log(Level.WARNING, "[Warriors] someone attempted to set " +((Player)super.getOwner()).getName()+ " to race " + characterName + " which doesn't exist.");
        }
        this.characterName = characterName;
        characters[1] = WarriorsManager.getWarCharacterLoader().getWarCharacter(characterName);
        writeToDatabase();
    }

    /**
     * Get this players WarCharacter class
     * @return the WarCharacter
     */
    public WarClass getPrimaryClass() {
        return characters[0];
    }
    
    /**
     * Get this players Secondary WarCharacter class
     * @return the WarCharacter, Null if they don't have one.
     */
    public WarClass getSecondaryClass() {
        return characters[1];
    }

    /**
     * checks if a player can use a skill. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param skillName name of the skill
     * @return whether or not they can use the skill
     */
    public boolean canPrimaryUseSkill(String skillName) {
        if(!characters[0].canClassUseSkill(skillName)){
           return false;
        }
        if(characters[0].getSkillLevel(skillName) > ((Player)super.getOwner()).get(XpComponent.class).getLevel()){
            return false;
        }
        AchievementLoader loader = WarriorsManager.getAchievementLoader();
        for(Achievement a : characters[0].getSkillAchievements(skillName)){
            if(!loader.hasPlayerAchieved(((Player)super.getOwner()), skillName)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * checks if a player can use a skill. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param skillName name of the skill
     * @return whether or not they can use the skill
     */
    public boolean canSecondaryUseSkill(String skillName) {
        if(characters[1] == null)return false;
        if(!characters[1].canClassUseSkill(skillName)){
           return false;
        }
        if(characters[1].getSkillLevel(skillName) > ((Player)super.getOwner()).get(XpComponent.class).getLevel()){
            return false;
        }
        AchievementLoader loader = WarriorsManager.getAchievementLoader();
        for(Achievement a : characters[1].getSkillAchievements(skillName)){
            if(!loader.hasPlayerAchieved(((Player)super.getOwner()), skillName)){
                return false;
            }
        }
        return true;
    }

    /**
     * checks if a player can use a skill. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param skill instance of the skill
     * @return whether or not they can use the skill
     */
    public boolean canPrimaryUseSkill(Skill skill) {
        return canSecondaryUseSkill(skill.getname());
    }
    
    /**
     * checks if a player can use a skill. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param skill instance of the skill
     * @return whether or not they can use the skill
     */
    public boolean canSecondaryUseSkill(Skill skill) {
        return canSecondaryUseSkill(skill.getname());
    }
    
    /**
     * checks if a player can use a item. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param itemName name of the item
     * @return whether or not they can use the item
     */
    public boolean canPrimaryUseItem(String itemName) {
        if(!characters[0].canClassUseTool(itemName)){
            return false;
        }
        if(characters[0].getItemLevel(itemName) > ((Player)super.getOwner()).get(XpComponent.class).getLevel()){
            return false;
        }
        AchievementLoader loader = WarriorsManager.getAchievementLoader();
        for(Achievement a : characters[0].getSkillAchievements(itemName)){
            if(!loader.hasPlayerAchieved(((Player)super.getOwner()), itemName)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * checks if a player can use a item. checks using levels and/or achievements.  
     * Also checks if this players WarCharacter is allowed to use it.
     * @param itemName name of the item
     * @return whether or not they can use the item
     */
    public boolean canSecondaryUseItem(String itemName) {
        if(characters[1] == null)return false;
        if(!characters[1].canClassUseTool(itemName)){
            return false;
        }
        if(characters[1].getItemLevel(itemName) > ((Player)super.getOwner()).get(XpComponent.class).getLevel()){
            return false;
        }
        AchievementLoader loader = WarriorsManager.getAchievementLoader();
        for(Achievement a : characters[1].getSkillAchievements(itemName)){
            if(!loader.hasPlayerAchieved(((Player)super.getOwner()), itemName)){
                return false;
            }
        }
        return true;
    }

    /**
     * get the player this component is attached to
     * @return the player
     */
    public Player getPlayer() {
        return ((Player)super.getOwner());
    }

    /**
     * see if this player has a Race; should be true....
     * @return whether or not the player has a Race
     */
    public boolean hasRace() {
        return race == null ? false : true;
    }

    /**
     * get this players race
     * @return the Race
     */
    public Race getRace() {
        return race;
    }

    /**
     * get the name of this player's Race
     * @return race name
     */
    public String getRaceName(){
        return raceName;
    }

    /**
     * remove this players race
     */
    public void removeRace() {
        raceName = "None";
        race = null;
        writeToDatabase();
    }

    /**
     * set this players race
     * @param raceName name to set the race to.
     */
    public void setRace(String raceName) {
        if(!WarriorsManager.getRaceLoader().isRaceLoaded(raceName)){
            Spout.getLogger().log(Level.WARNING, "[Warriors] someone attempted to set " +((Player)super.getOwner()).getName()+ " to race " + raceName + " which doesn't exist.");
        }
        this.raceName = raceName;
        race = WarriorsManager.getRaceLoader().getRace(raceName);
        writeToDatabase();
    }

    @Override
    public void refreshComponent() {
        writeToDatabase();
        isLoaded = true;
        if(mysql.isKeyExists("SELECT * FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'")){
            readFromDatabase();
            
            characters[0] = WarriorsManager.getWarCharacterLoader().getWarCharacter(characterName);
            if(!character2Name.trim().equals("")){
                characters[1] = WarriorsManager.getWarCharacterLoader().getWarCharacter(characterName);
            }
            race = WarriorsManager.getRaceLoader().getRace(raceName);
            if(Spout.debugMode()){
                Warriors.getLog().info("Loaded Player " + ((Player)super.getOwner()).getName());
                Spout.getLogger().log(Level.INFO,"Class: " + characters);
                Spout.getLogger().log(Level.INFO,"Race: " + race);
            }
            if(health != null){
                health.refreshComponent();
            }
            else{
                health = new WarriorsHealth(((Player)super.getOwner()));
                if(Spout.debugMode())
                    Warriors.getLog().info(" Health component added.");
            }
            if (((Player)super.getOwner()).get(ManaComponent.class) != null){
                ((Player)super.getOwner()).get(ManaComponent.class).refreshComponent();
            }
            else {
                ((Player)super.getOwner()).add(ManaComponent.class).refreshComponent();
            }
            if(((Player)super.getOwner()).get(StaminaComponent.class) != null){
                ((Player)super.getOwner()).get(StaminaComponent.class).refreshComponent();
            }
            else {
                ((Player)super.getOwner()).add(StaminaComponent.class).refreshComponent();
            }
            if (((Player)super.getOwner()).get(ToolComponent.class) != null){
                ((Player)super.getOwner()).get(ToolComponent.class).refreshComponent();
            }
            else {
                ((Player)super.getOwner()).add(ToolComponent.class).refreshComponent();
            }
            if (((Player)super.getOwner()).get(XpComponent.class) != null){
                ((Player)super.getOwner()).get(XpComponent.class).refreshComponent();
            }
            else {
                ((Player)super.getOwner()).add(XpComponent.class).refreshComponent();
            }
            if (((Player)super.getOwner()).get(SkillPointComponent.class) != null){
                ((Player)super.getOwner()).add(SkillPointComponent.class);
            }
            else {
                ((Player)super.getOwner()).add(SkillPointComponent.class).refreshComponent();
            }
        }
    }





}
