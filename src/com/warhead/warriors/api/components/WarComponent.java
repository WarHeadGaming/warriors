/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.components;

/**
 *
 * @author somners
 */
public interface WarComponent {

    public void readFromDatabase();

    public void writeToDatabase();

    public void refreshComponent();

}
