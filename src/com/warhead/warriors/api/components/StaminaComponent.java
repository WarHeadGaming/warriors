
package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.WarriorsConfig;
import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.api.trigger.PlayerStaminaChangeTrigger;
import com.warhead.warriors.api.trigger.TriggerManager;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class StaminaComponent extends EntityComponent implements WarComponent{

    private int stamina = 0;
    private int staminaSP = 0;
    private int staminaRegenSP = 0;
    private int staminaFull = 0;
    private int staminaBase = 0;
    private int staminaMax = 0;
    private int staminaModifier = 0;
    private MySQLManager mysql;
    private boolean isLoaded = false;
    private int lastUpdate = 0;


    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }

    @Override
    public void refreshComponent(){
        writeToDatabase();
        isLoaded = true;
        readFromDatabase();
        staminaModifier = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getSPStaminaModifier();
        staminaMax = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getMaxStamina();
        staminaBase = ((Player)super.getOwner()).get(RPGComponent.class).getRace().getBaseStamina();
        int tempStaminaFull = ((Player)super.getOwner()).get(RPGComponent.class).hasRace() ? ( staminaBase + (staminaModifier * staminaSP ) ) : ( staminaBase + (staminaModifier * staminaSP) ) ;
        staminaFull = tempStaminaFull <= staminaMax ? tempStaminaFull : staminaMax;
    }

    @Override
    public void readFromDatabase() {
        stamina = mysql.getIntValue("SELECT `stamina` FROM `warriors_players` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "stamina");
        staminaSP = mysql.getIntValue("SELECT `stamina` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "stamina");
        staminaRegenSP = mysql.getIntValue("SELECT `stamina_regen` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "stamina_regen");
    }

    @Override
    public void writeToDatabase() {
        if(isLoaded){
            mysql.update("UPDATE `warriors_players` SET `stamina` = '"+ stamina +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            mysql.update("UPDATE `warriors_skill_points` SET `stamina` = '"+ staminaSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
            mysql.update("UPDATE `warriors_skill_points` SET `mana_regen` = '"+ staminaRegenSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
        }
    }

    @Override
    public void onTick(float dt){
        ++lastUpdate;
        Race race = ((Player)super.getOwner()).get(RPGComponent.class).getRace();
        if(race.getStaminaRegenInterval() <= lastUpdate/20 && getStamina() <= getMaxStamina()){
            lastUpdate = 0;
            float percent = (race.getStaminaSPRegenModifier() * staminaRegenSP) + race.getStaminaRegenRate();
            addStamina((int)percent*staminaFull >= 1 ? (int)percent*staminaFull : 1, StaminaChangeCause.REGEN);
            ((Player)super.getOwner()).sendMessage(WarriorsConfig.getServerHeader().append(" Stamina: " + getStamina() + "/" + getFullStaminaValue()));
        }
    }
    
    public int getStamina() {
        return stamina;
    }


    public int getMaxStamina() {
        return staminaMax;
    }


    public void setStamina(int toSet, StaminaChangeCause source) {
        if(toSet > staminaFull || toSet < 0) {return;}
        PlayerStaminaChangeTrigger pmct = new PlayerStaminaChangeTrigger(((Player)super.getOwner()), stamina, toSet, source);
        if(Spout.debugMode())
            Warriors.getLog().info("Set Stamina to Player " + ((Player)super.getOwner()).getName() + " to " + toSet);
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        if(toSet > staminaFull){stamina = staminaFull;}
        else if(toSet < 0){stamina = 0;}
        else{stamina = toSet;}
    }


    public void setMaxStamina(int toSet) {
        staminaMax = toSet;
    }


    public int getSPStaminaModifier(){
        return staminaModifier;
    }


    public void addStamina(int toAdd, StaminaChangeCause source) {
        PlayerStaminaChangeTrigger pmct = new PlayerStaminaChangeTrigger(((Player)super.getOwner()), stamina, stamina + toAdd, source);
        if(Spout.debugMode())
            Warriors.getLog().info("Added " + toAdd + " Stamina to Player " + ((Player)super.getOwner()));
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        stamina = (stamina + toAdd) >= staminaFull ? staminaFull : stamina + toAdd;
    }


    public void removeStamina(int toRemove, StaminaChangeCause source) {
        PlayerStaminaChangeTrigger pmct = new PlayerStaminaChangeTrigger(((Player)super.getOwner()), stamina, stamina - toRemove, source);
        if(Spout.debugMode())
        Warriors.getLog().info("Removed " + toRemove + " Stamina to Player " + ((Player)super.getOwner()));
        TriggerManager.processListenersForExecute(pmct);
        if(pmct.isTriggerEventCanceled()){return;}
        stamina = (stamina - toRemove) <= 0 ? 0 : stamina - toRemove;
    }

    public int getStaminaPoints() {
        return staminaSP;
    }

    public int getFullStaminaValue(){
        return ( staminaBase + (staminaModifier*staminaSP) ) <= staminaMax ? ( staminaBase + (staminaModifier*staminaSP) ) : staminaMax;
    }

    public void addStaminaPoint() {
        staminaSP++;
        refreshComponent();
    }


    public void removeStaminaPoint() {
        staminaSP--;
        refreshComponent();
    }

    public void addStaminaPoints(int toAdd){
        staminaSP = staminaSP + toAdd;
        refreshComponent();
    }

    public void removeStaminaPoints(int toRemove){
        staminaSP = staminaSP - toRemove < 0 ? 0 : staminaSP - toRemove;
        refreshComponent();
    }
    
    public void addStaminaRegenPoint() {
        staminaRegenSP++;
        refreshComponent();
    }


    public void removeStaminaRegenPoint() {
        staminaRegenSP--;
        refreshComponent();
    }

    public void addStaminaRegenPoints(int toAdd){
        staminaRegenSP = staminaRegenSP + toAdd;
        refreshComponent();
    }

    public void removeStaminaRegenPoints(int toRemove){
        staminaRegenSP = staminaRegenSP - toRemove < 0 ? 0 : staminaRegenSP - toRemove;
        refreshComponent();
    }

    public void setStaminaPoint(int toSet) {
        staminaSP = toSet;
    }

    public boolean isStaminaMaxedOut(){
        if(staminaMax < (staminaBase + (staminaSP*staminaModifier))){
            return false;
        }
        return true;
    }


}