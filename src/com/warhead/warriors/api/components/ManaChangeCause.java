
package com.warhead.warriors.api.components;

/**
 *
 * @author Somners
 */
public enum ManaChangeCause {
    
    PLUGIN("Plugin",1),
    SKILL("Skill",2),
    ENEMYSKILL("Enemy Skill",3),
    REGEN("Regeneration",4),
    UNKNOWN("Unknown",5);
    
    private String name;
    private int id;

    ManaChangeCause(String name, int id){
        this.name = name;
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }

    public static int getId(String toolName){
        int id = 0;
        for(ManaChangeCause type : ManaChangeCause.values()){
            if(type.getName().equals(toolName)){
                id = type.getId();
                break;
            }
        }
        return id;
    }
}
