package com.warhead.warriors.api.components;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.warcharacter.WarClass;
import java.util.List;
import org.spout.api.component.type.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class ToolComponent extends EntityComponent implements WarComponent{
    
    private int damageSP = 0;
    private MySQLManager mysql;
    private boolean loaded = false;
    
    @Override
    public void onAttached(){
        super.onAttached();
        mysql = Warriors.getDatabase();
    }

    @Override
    public void onDetached(){

    }

    @Override
    public void refreshComponent(){
        if(!loaded){
            readFromDatabase();
        }
        else{
            writeToDatabase();
        }
    }

    @Override
    public void readFromDatabase() {
        damageSP = mysql.getIntValue("SELECT `tool_damage` FROM `warriors_skill_points` WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"'", "tool_damage");
    }

    @Override
    public void writeToDatabase() {
        mysql.update("UPDATE `warriors_skill_points` SET `tool_damage` = '"+ damageSP +"' WHERE `player_name` = '"+ ((Player)super.getOwner()).getName() +"' LIMIT 1");
    }
    
    public int getDamageModifier(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getItemDamageModifier(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getItemDamageModifier(itemName);
        }
        return -1;
    }
    
    public int getMaxDamage(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getItemMaxDamage(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getItemMaxDamage(itemName);
        }
        return -1;
    }
    
    public int getBaseDamage(String itemName){
        RPGComponent rpg = ((Player)super.getOwner()).get(RPGComponent.class);
        if(rpg.hasSecondaryClass() && this.canSecondaryClassUseItem(itemName)){
            return rpg.getSecondaryClass().getItemBaseDamage(itemName);
        }
        if(this.canPrimaryClassUseItem(itemName)){
            return rpg.getPrimaryClass().getItemBaseDamage(itemName);
        }
        return -1;
    }
    
    public int getDamageSP(){
        return damageSP;
    }
    
    public void addDamageSP(int toAdd){
        damageSP = damageSP + toAdd;
    }
    
    public void removeDamageSP(int toRemove){
        damageSP = damageSP - toRemove < 0 ? 0 : damageSP - toRemove;
    }
    
    public void setDamageSP(int toSet){
        damageSP = toSet;
    }
    
    public boolean canPrimaryClassUseItem(String itemName){
        if(!((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass().canClassUseTool(itemName)){
            return false;
        }
        return ((Player)super.getOwner()).get(RPGComponent.class).canPrimaryUseItem(itemName);
    }
    
    public boolean canSecondaryClassUseItem(String itemName){
        if(!((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass().canClassUseTool(itemName)){
            return false;
        }
        return ((Player)super.getOwner()).get(RPGComponent.class).canSecondaryUseItem(itemName);
    }
    
    public int getDamage(String itemName){
        int max = this.getBaseDamage(itemName);
        int min = this.getMaxDamage(itemName);
        int mod = this.getDamageModifier(itemName);
        return min + (mod * this.damageSP) > max ? max : min + (mod * this.damageSP);
    }
    
    public boolean isDamageSPMaxed(){
        WarClass character = ((Player)super.getOwner()).get(RPGComponent.class).getPrimaryClass();
        WarClass scharacter = ((Player)super.getOwner()).get(RPGComponent.class).getSecondaryClass();
        List<String> tools = character.getToolNames();
        for(String wName : tools){
            if(scharacter != null && this.canSecondaryClassUseItem(wName)){
                int mod = scharacter.getItemDamageModifier(wName);
                int max = scharacter.getItemBaseDamage(wName);
                int min = scharacter.getItemMaxDamage(wName);
                if(max > min + (mod*damageSP)){
                    return false;
                }
            }
            else if(this.canPrimaryClassUseItem(wName)){
                int mod = character.getItemDamageModifier(wName);
                int max = character.getItemBaseDamage(wName);
                int min = character.getItemMaxDamage(wName);
                if(max > min + (mod*damageSP)){
                    return false;
                }
            }
        }
        return true;
    }
}
