package com.warhead.warriors.api.trigger;

import org.spout.api.entity.Entity;
import org.spout.api.entity.Player;
import org.spout.api.event.Cause;

/**
 *  Trigger for when the player is damaged
 * @author somners
 */
public class PlayerDefendTrigger extends Trigger{

    private static TriggerType type = TriggerType.PlayerDefend;
    private Player player;
    private Entity attacker;
    private int damage;
    private boolean isCancelled = false;
    private Cause<?> cause;

    public PlayerDefendTrigger(Player player, Entity attacker, int damage, Cause<?> damageCause){
        this.player = player;
        this.attacker = attacker;
        this.damage = damage;
        this.cause = damageCause;
    }

    /**
     * set whether or not to cancel the Spout event this trigger represents
     * @param isCanceled true = cancel the event<br>
     * false = do not cancel the event
     */
    @Override
    public void setTriggerEventCanceled(boolean isCanceled) {
        isCancelled = isCanceled;
    }

    /**
     * cancels the Spout event this trigger represents
     */
    @Override
    public void cancelTriggerEvent() {
        isCancelled = true;
    }

    /**
     * check it the Spout event this trigger represents is canceled or not
     * @return true = the event is canceled<br>
     * false = the event is not canceled
     */
    @Override
    public boolean isTriggerEventCanceled() {
        return isCancelled;
    }

    /**
     * get the type of this trigger
     * @return the TriggerType of this trigger
     */
    @Override
    public TriggerType getTriggerType() {
        return type;
    }

    /**
     * gets the player attacking
     * @return player
     */
    @Override
    public Player getPlayer(){
        return player;
    }

    public Entity getAttacker(){
        return attacker;
    }
    
    /**
     * gets the cause of damage
     * @return damage source
     */
    public Cause<?> getDamageCause(){
        return cause;
    }

    /**
     * gets the amount of damage done to the player
     * @return damage amount
     */
    public int getDamage(){
       return damage;
    }

    /**
     * set the amount of damage done to the player
     * @param change damage amount
     */
    public void setDamage(int change){
        damage = change;
    }

}
