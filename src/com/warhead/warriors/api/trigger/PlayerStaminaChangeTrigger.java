
package com.warhead.warriors.api.trigger;

import com.warhead.warriors.api.components.ManaChangeCause;
import com.warhead.warriors.api.components.StaminaChangeCause;
import com.warhead.warriors.api.components.StaminaComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public class PlayerStaminaChangeTrigger extends Trigger{

    private Player player;
    private int oldStamina;
    private int newStamina;
    private StaminaChangeCause source;
    private boolean isCanceled = false;
    private static final TriggerType type = TriggerType.PlayerStaminaChange;


    public PlayerStaminaChangeTrigger(Player player, int oldStamina, int newStamina, StaminaChangeCause source){
        this.player = player;
        this.oldStamina = oldStamina;
        this.newStamina = newStamina;
        this.source = source;
    }

    @Override
    public void setTriggerEventCanceled(boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    @Override
    public void cancelTriggerEvent() {
        isCanceled = true;
    }

    @Override
    public boolean isTriggerEventCanceled() {
        return isCanceled;
    }

    @Override
    public TriggerType getTriggerType() {
        return type;
    }

    public int getOldStamina(){
        return oldStamina;
    }

    public int getNewStamina(){
        return newStamina;
    }

    public int getStaminaChange(){
        return Math.abs(oldStamina - newStamina);
    }

    public StaminaChangeCause getSource(){
        return source;
    }

    @Override
    public Player getPlayer(){
        return player;
    }

    public StaminaComponent getStaminaComponent(){
        return player.get(StaminaComponent.class);
    }

}
