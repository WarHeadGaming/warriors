package com.warhead.warriors.api.trigger;

/**
 * enum to differentiate between the different trigger types
 * @author somners
 */
public enum TriggerType {

    PlayerAttack,
    PlayerDefend,
    PlayerRightClick,
    PlayerLeftClick,
    PlayerCrouch,
    PlayerManaChange,
    PlayerStaminaChange;
}
