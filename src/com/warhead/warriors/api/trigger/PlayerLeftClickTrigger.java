package com.warhead.warriors.api.trigger;

import org.spout.api.entity.Player;
import org.spout.api.event.player.PlayerInteractEvent;
import org.spout.api.geo.discrete.Point;
import org.spout.api.inventory.ItemStack;

/**
 *
 * @author somners
 */
public class PlayerLeftClickTrigger extends Trigger{

    public static TriggerType type = TriggerType.PlayerLeftClick;
    private Player player;
    private PlayerInteractEvent event;

    public PlayerLeftClickTrigger(PlayerInteractEvent event){
        this.player = event.getPlayer();
        this.event = event;
    }

    /**
     * set whether or not to cancel the Spout event this trigger represents
     * @param isCanceled true = cancel the event<br>
     * false = do not cancel the event
     */
    @Override
    public void setTriggerEventCanceled(boolean isCanceled) {
        event.setCancelled(isCanceled);
    }

    /**
     * cancels the Spout event this trigger represents
     */
    @Override
    public void cancelTriggerEvent() {
        event.setCancelled(true);
    }

    /**
     * check it the Spout event this trigger represents is canceled or not
     * @return true = the event is canceled<br>
     * false = the event is not canceled
     */
    @Override
    public boolean isTriggerEventCanceled() {
        return event.isCancelled();
    }

    /**
     * get the type of this trigger
     * @return the TriggerType of this trigger
     */
    @Override
    public TriggerType getTriggerType() {
        return type;
    }

    /**
     * gets the player attacking
     * @return player
     */
    @Override
    public Player getPlayer(){
        return player;
    }

    /**
     * gets the item held in the players hand
     * @return the item
     */
    public ItemStack getHeldItem(){
        return event.getHeldItem();
    }

    /**
     * gets the location of the interaction
     * @return the Point
     */
    public Point getInteractedPoint(){
        return event.getInteractedPoint();
    }

    /**
     * checks if the interacted point is an air block
     * @return
     */
    public boolean isAir(){
        return event.isAir();
    }

}