package com.warhead.warriors.api.trigger;

import org.spout.api.entity.Entity;
import org.spout.api.entity.Player;
/**
 *  Trigger for when the player attacks an entity
 * @author somners
 */
public class PlayerAttackTrigger extends Trigger{

    /**
     *
     */
    public static TriggerType type = TriggerType.PlayerAttack;
    private Player player;
    private Entity defender;
    private boolean isCancelled = false;
    private int damage = 0;

    /**
     *
     * @param event
     * @param player
     */
    public PlayerAttackTrigger(Player player, Entity defender, int damage){
        this.player = player;
        this.defender = defender;
        this.damage = damage;
    }

    /**
     * set whether or not to cancel the Spout event this trigger represents
     * @param isCanceled true = cancel the event<br>
     * false = do not cancel the event
     */
    @Override
    public void setTriggerEventCanceled(boolean isCanceled) {
        isCancelled = isCanceled;
    }

    /**
     * cancels the Spout event this trigger represents
     */
    @Override
    public void cancelTriggerEvent() {
        isCancelled = true;
    }

    /**
     * check it the Spout event this trigger represents is canceled or not
     * @return true = the event is canceled<br>
     * false = the event is not canceled
     */
    @Override
    public boolean isTriggerEventCanceled() {
        return isCancelled;
    }

    /**
     * get the type of this trigger
     * @return the TriggerType of this trigger
     */
    @Override
    public TriggerType getTriggerType() {
        return type;
    }

    /**
     * gets the player attacking
     * @return player
     */
    @Override
    public Player getPlayer(){
        return player;
    }

    /**
     * gets the entity being attacked
     * @return defending entity
     */
    public Entity getDefendingEntity(){
        return defender;
    }

    /**
     * set the damage done to the defending entity
     * @param change damage amount
     */
    public void setDefenderDamage(int change){
        damage = change;
    }

    /**
     * get the damage done to the defending entity
     * @return the damage amount
     */
    public int getDefenderDamage(){
        return damage;
    }
    
}
