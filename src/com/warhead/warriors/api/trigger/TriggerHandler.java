package com.warhead.warriors.api.trigger;

/**
 *This is the annotation to denote methods as a trigger method<br>
 * Put the annotation "@TriggerHandler" before your method for<br>
 * a skill Trigger. <br>
 * example:<br>
 * @TriggerHandler
 * public void onPlayerAttack(PlayerAttackTrigger trigger){<br>
 *      //your skill code goes here<br>
 * }<br>
 *
 * @author somners
 */
import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TriggerHandler {

}
