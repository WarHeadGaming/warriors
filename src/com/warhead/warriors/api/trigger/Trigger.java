package com.warhead.warriors.api.trigger;

import org.spout.api.entity.Player;

/**
 * base class of all Triggers
 * @author somners
 */
public abstract class Trigger {

    public abstract void setTriggerEventCanceled(boolean isCanceled);

    public abstract void cancelTriggerEvent();

    public abstract boolean isTriggerEventCanceled();

    public abstract TriggerType getTriggerType();
    
    public abstract Player getPlayer();

}
