/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.achievement;

import java.util.HashMap;

/**
 *
 * @author somners
 */
public interface AchievementDescriptionFile {

    public String getName();

    public String getMainClass();

    public HashMap<String, Object> getWarCharacterConfigEntry();

}
