/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.achievement;

import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public abstract class Achievement {
    
    public abstract void onEnable();
    
    public abstract void onDisable();
    
    public abstract boolean hasPlayerAchieved(Player player);
}
