/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api.achievement;

import java.io.File;
import java.util.List;
import org.spout.api.entity.Player;

/**
 *
 * @author somners
 */
public interface AchievementLoader {

    public boolean hasPlayerAchieved(Player player, String achievement);
    
    /**
     * checks if the achievement is loaded
     * @param achievementName name of the achievement
     * @return true = the achievement is loaded<br>
     * false = the achievement is not loaded
     */
    public boolean isAchievementLoaded(String achievementName);

    /**
     * gets a List() of names of all achievements loaded
     * @return the List of achievement names
     */
    public List<String> getAchievementNames();

    /**
     * Gets a List() of Achievements
     * @return the List of Achievements
     */
    public List<Achievement> getAchievements();

    /**
     * Gets a single Achievements based on the Name of the Achievement
     * @param achievementName Name of the Achievement as String
     * @return the Achievement
     */
    public Achievement getAchievement(String achievementName);

    /**
     * Loads an Achievement based on the Name of the Achievement
     * @param achievementName Name of the Achievement as String
     */
    public void loadAchievement(String achievementName);

    /**
     * Loads an Achievement JAR file based on the File path given
     * 
     * ex: new File(path/to/achievement.jar);
     * 
     * @param achievementJar File to Achievement JAR
     */
    public void loadAchievement(File achievementJar);

    /**
     *Loads all Achievements avaliable
     */
    public void loadAllAchievements();

    /**
     * Unload Achievement based on Achievement name
     * @param achievementName Name of the Achievement as String
     */
    public void unloadAchievement(String achievementName);

    /**
     * Reloads all Achievements avaliable. Used for dynamic reloading when new 
     * Achievement is installed
     */
    public void reloadAllAchievements();

    /**
     * Reload a single Achievement based on Achievements name. Used for dynamic
     * reloading of one Achievement
     * @param achievementName Name of the Achievement as String
     */
    public void reloadAchievement(String achievementName);

    /**
     * 
     * @param achievementName
     * @return
     */
    public AchievementDescriptionFile getAchievementDescriptionFile(String achievementName);

}
