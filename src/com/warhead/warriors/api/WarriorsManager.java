/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.api;

import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.CoolDown;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.achievement.WAchievementlLoader;
import com.warhead.warriors.api.Util.ItemHandler;
import com.warhead.warriors.api.achievement.AchievementLoader;
import com.warhead.warriors.api.races.RaceLoader;
import com.warhead.warriors.api.skill.Skill;
import com.warhead.warriors.api.skill.SkillLoader;
import com.warhead.warriors.api.trigger.TriggerListener;
import com.warhead.warriors.api.trigger.TriggerManager;
import com.warhead.warriors.api.warcharacter.WarClassLoader;
import com.warhead.warriors.character.WClassLoader;
import com.warhead.warriors.race.WRaceLoader;
import com.warhead.warriors.skills.WSkillLoader;
import java.util.logging.Level;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class WarriorsManager {

    private static MySQLManager mysql = Warriors.getDatabase();
    
    /**
     * gets the WarCharacter Loader
     * @return the WarCharacter Loader
     */
    public static WarClassLoader getWarCharacterLoader(){
        return new WClassLoader();
    }

    /**
     * gets the WarCharacter Loader
     * @return the WarCharacter Loader
     */
    public static RaceLoader getRaceLoader(){
        return new WRaceLoader();
    }

    /**
     * gets the skill loader
     * @return the skill loader
     */
    public static SkillLoader getSkillLoader(){
        return new WSkillLoader();
    }
    
    /**
     * gets the achievement loader
     * @return the achievement loader
     */
    public static AchievementLoader getAchievementLoader(){
        return new WAchievementlLoader();
    }

    /**
     * register a TriggerListener so it can get called upon
     * @param skill instance of the Skill plugin it is part of
     * @param listener the instance of the listener class
     */
    public static void registerTriggerListener(Skill skill, TriggerListener listener){
        TriggerManager.registerListener(listener, skill);
    }

    /**
     * unregister a TriggerListener so it won't be called upon anymore
     * @param skill skill you want to unregister listeners for.
     */
    public static void unRegisterTriggerListener(Skill skill){
        TriggerManager.unregisterListener(skill);
    }
    
    public static boolean doesPlayerHaveCharacter(String playerName){
        return mysql.isKeyExists("SELECT * FROM `warriors_players` WHERE `player_name` = '"+playerName+"'");
    }
    
    public static void createNewWarrior(String playerName, String characterName, String raceName){
        if(!mysql.isKeyExists("SELECT * FROM `warriors_players` WHERE `player_name` = '"+playerName+"'")){
            mysql.insert("INSERT INTO `warriors_players` ( `player_name`, `character_name`, "
                + "`race_name`, `xp`, `mana`, `stamina`, `race_changes`) VALUES ('"+playerName+"', "
                + "'"+characterName+"', '"+raceName+"', '0', '0', '0', '0')");
            mysql.insert("INSERT INTO `warriors_skill_points` (`player_name`,"
                + " `tool`, `health`, `mana`, `stamina`, `tool_damage`, `mana_regen`, "
                + "`stamina_regen`, `health_regen`, `unused`) VALUES ('"+playerName+"',"
                + " '0', '0', '0', '0', '0', '0', '0', '0',l '0')");
            
            
            Warriors.getLog().info(" Character: "+playerName+" of class "+characterName+" with race "+raceName+" has been created!");
            return;
        }
        Warriors.getLog().info(" Error creating Character for: "+playerName+". Do they already have one?");
    }
    
    /**
     * get the active ToolManager for this game
     * @return 
     */
    public static ItemHandler getToolManager(){
        return Warriors.getItemHandler();
    }
    
    /**
     * get the cooldown manager
     * @return cooldown manager
     */
    public static CoolDown getCooldownManager(){
        return Warriors.getCooldownManager();
    }

    public static SkillBindManager getSkillBindManager(){
        return new SkillBindManager();
    }
    
}
