package com.warhead.warriors.api.skill;

import java.io.File;
import java.util.List;

/**
 *
 * @author somners
 */
public interface SkillLoader {

    /**
     * checks if the skill is loaded
     * @param skillName name of the skill
     * @return true = the skill is loaded<br>
     * false = the skill is not loaded
     */
    public boolean isSkillLoaded(String skillName);

    /**
     * gets a List() of names of all skills loaded
     * @return the List of skill names
     */
    public List<String> getSkillNames();

    /**
     *
     * @return
     */
    public List<Skill> getSkills();

    /**
     *
     * @param skillName
     * @return
     */
    public Skill getSkill(String skillName);

    /**
     *
     * @param skillName
     */
    public void loadSkill(String skillName);

    /**
     *
     * @param skillJar
     */
    public void loadSkill(File skillJar);

    /**
     *
     */
    public void loadAllSkills();

    /**
     *
     * @param skillName
     */
    public void unloadSkill(String skillName);

    /**
     *
     */
    public void reloadAllSkills();

    /**
     *
     * @param skillName
     */
    public void reloadSkill(String skillName);

    /**
     *
     * @param skillName
     * @return
     */
    public SkillDescriptionFile getSkillDescriptionFile(String skillName);

}
