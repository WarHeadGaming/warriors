package com.warhead.warriors.api.skill;

/**
 *
 * @author somners
 */
public abstract class Skill {

    public String skillName = "";

    /**
     * called when the skill is loaded/enabled
     */
    public abstract void onEnable();

    /**
     * called when the skill is unloaded/disabled
     */
    public abstract void onDisable();

    /**
     * gets the name of this skill
     * @return the skill name
     */
    public String getname(){
        return skillName;
    }

    /**
     * sets the name of this skill
     * @param name  the skill name
     */
    public void setName(String name){
        skillName = name;
    }


}
