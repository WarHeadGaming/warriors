
package com.warhead.warriors.achievement;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.achievement.Achievement;
import com.warhead.warriors.api.achievement.AchievementDescriptionFile;
import com.warhead.warriors.api.achievement.AchievementLoader;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.Spout;
import org.spout.api.entity.Player;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author somners
 */
public class WAchievementlLoader implements AchievementLoader {

    private static HashMap<String, Achievement> achievements = new HashMap<String, Achievement>();
    private static File achievementDir = new File("plugins/Warriors/Achievements/");
    
    @Override
    public boolean hasPlayerAchieved(Player player, String achievement){
        if(!achievements.containsKey(achievement)){
            Warriors.getLog().severe(" unable to find Achievement: " + achievement);
            return false;
        }
        return achievements.get(achievement).hasPlayerAchieved(player);
    }
    
    @Override
    public boolean isAchievementLoaded(String achievementName){
        return achievements.containsKey(achievementName);
    }

    @Override
    public List<String> getAchievementNames(){
        List<String> achievements = new ArrayList();
        String[] achievementsArray = (String[])this.achievements.keySet().toArray();
        for(int i = 0 ; achievementsArray.length < i ; i++){
            achievements.add(achievementsArray[i]);
        }
        return achievements;
    }

    @Override
    public List<Achievement> getAchievements(){
        List<Achievement> achievements = new ArrayList();
        Achievement[] achievementsArray = (Achievement[])this.achievements.values().toArray();
        for(int i = 0 ; achievementsArray.length < i ; i++){
            achievements.add(achievementsArray[i]);
        }
        return achievements;
    }

    @Override
    public Achievement getAchievement(String achievementName){
        return achievements.get(achievementName);
    }

    @Override
    public void loadAchievement(String achievementName) {
        String[] list = achievementDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(achievementName+".jar")){
                loadAchievement(new File(achievementDir.getPath() + achievementName + ".jar"));
            }
        }

    }

    @Override
    public void loadAchievement(File file){
        InputStream in = null;
        String main = null;
        String name = null;
        Achievement achievement = null;
        try {
        
            ClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()}, Warriors.getPlugin().getClassLoader());
            URL achievementDescURL = loader.getResource("Achievement.yml");
            URLConnection achievementDescConn = achievementDescURL.openConnection();
            
            Warriors.getLog().info(achievementDescConn.toString());
            
            in = achievementDescConn.getInputStream();
            
            Warriors.getLog().info(achievementDescConn.getURL().toString());
            Warriors.getLog().info(achievementDescConn.getContent().toString());
            
            WAchievementDescriptionFile achievementDesc = new WAchievementDescriptionFile(new YamlConfiguration(in));
            main = achievementDesc.getMainClass();
            
            Warriors.getLog().info("Main = " + main);
            name = achievementDesc.getName();
            Class<?> achievementMain = loader.loadClass(main);
            achievement = (Achievement) achievementMain.newInstance();
            
            
        } catch (InstantiationException ex) {
            Logger.getLogger(WAchievementlLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WAchievementlLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WAchievementlLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Warriors.getLog().severe(" Read error loading achievement:");
            Warriors.getLog().severe(" " + ex.getMessage());
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Warriors.getLog().severe(" Close error post-loading a achievement:");
                Warriors.getLog().severe(" " + ex.getMessage());
            }
        }
        if(name == null){
            Warriors.getLog().severe(" Invalid or no name loading achievement with main class '"+main+"'");
            return;
        }
        if(achievement == null){
            Warriors.getLog().severe(" Something went wrong loading achievement with main class '"+main+"'");
            return;
        }
        achievements.put(name, achievement);
        achievement.onEnable();
        Warriors.getLog().severe(" Achievement '"+name+"' has been loaded.");
    }

    @Override
    public void loadAllAchievements() {
        String[] list = achievementDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".jar")){
                loadAchievement(new File(achievementDir, list[i]));
            }
        }
    }

    @Override
    public void unloadAchievement(String achievementName) {
        if(achievements.containsKey(achievementName)){
            achievements.remove(achievementName);
            Warriors.getLog().severe(" Achievement '"+achievementName+"' has been unloaded.");
            return;
        }
        Warriors.getLog().severe(" Achievement '"+achievementName+"' is not loaded and cannot be unloaded.");
    }

    @Override
    public void reloadAllAchievements() {
        achievements.clear();
        loadAllAchievements();
    }

    @Override
    public void reloadAchievement(String achievementName) {
        unloadAchievement(achievementName);
        loadAchievement(achievementName);
    }

    @Override
    public AchievementDescriptionFile getAchievementDescriptionFile(String achievementName) {
        String[] list = achievementDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(achievementName+".jar")){
                InputStream in = null;
                try {
                    File file = new File(achievementDir.getPath() + achievementName + ".jar");
                    URLClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()}, Thread.currentThread().getContextClassLoader());
                    URL achievementDescURL = loader.getResource("Achievement.yml");
                    URLConnection achievementDescConn = achievementDescURL.openConnection();
                    in = achievementDescConn.getInputStream();
                    return new WAchievementDescriptionFile(new YamlConfiguration(in));
                } catch (IOException ex) {
                    Logger.getLogger(WAchievementlLoader.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(WAchievementlLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return null;
    }
}
