/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.achievement;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.achievement.AchievementDescriptionFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.ConfigurationNode;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *Represents the Achievement.yml File<br>
 * <br>
 * Available Properties:<br>
 * # pound sign equals a commented out line<br>
 * # Set the main class extending Achievement.class<br>
 * main=<br>
 * # Set the name of this skill<br>
 * name=<br>
 *<br>
 * @author somners
 *
 */
public class WAchievementDescriptionFile implements AchievementDescriptionFile {

    private YamlConfiguration config;


    public WAchievementDescriptionFile(YamlConfiguration config) {
        try {
            this.config = config;
            this.config.load();
        } catch (ConfigurationException ex) {
            Warriors.getLog().info(" Error loading a Achievement Description File.");
        }
    }

    @Override
    public String getName() {
        return config.getNode("Name").getString();
    }

    @Override
    public String getMainClass() {
        return config.getNode("Main").getString();
    }

    @Override
    public HashMap<String, Object> getWarCharacterConfigEntry() {
        if(!doesEntryExist("Skill Config")){return null;}
        ConfigurationNode configNode = config.getNode("Skill config");
        HashMap<String, Object> returnMap = new HashMap<String, Object>();
        Iterator<String> it = configNode.getKeys(true).iterator();
        while(it.hasNext()){
            String node = it.next();
            returnMap.put(node.replace("Skill Config", getName()), config.getNode(node).getValue());
        }
        return returnMap;
    }

    public boolean doesEntryExist(String node) {
        ConfigurationNode testNode  = config.getNode(node);
        return testNode.getValue() == null ? false : true;
    }
}