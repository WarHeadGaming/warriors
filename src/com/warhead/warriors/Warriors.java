package com.warhead.warriors;

import com.warhead.warheadutils.WarHeadUtils;
import com.warhead.warheadutils.database.MySQLManager;
import com.warhead.warriors.api.Util.ItemHandler;
import com.warhead.warriors.api.Util.VanillaItemHandler;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.commands.WarriorsCommand;
import com.warhead.warriors.commands.WarriorsCommands;
import com.warhead.warriors.listeners.VanillaListener;
import com.warhead.warriors.listeners.WarriorsListener;
import com.warhead.warriors.listeners.WarriorsTriggerListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.spout.api.Spout;
import org.spout.api.chat.ChatArguments;
import org.spout.api.chat.style.ChatStyle;
import org.spout.api.command.CommandRegistrationsFactory;
import org.spout.api.command.annotated.AnnotatedCommandRegistrationFactory;
import org.spout.api.command.annotated.SimpleAnnotatedCommandExecutorFactory;
import org.spout.api.command.annotated.SimpleInjector;
import org.spout.api.event.Listener;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.plugin.PluginLogger;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author somners
 */
public class Warriors extends CommonPlugin{

    private static Thread mainThread;
    private static Warriors plugin;
    private static Listener spoutListener = null;
    private static Listener triggerListener = null;
    private static Listener vanillaTriggerListener = null;
    private static MySQLManager mysql;
    private WarriorsCommand wc;
    private WarriorsCommands wcs;
    private static WarriorsConfig config;
    private static ItemHandler itemHandler;
    private static CoolDown cooldown;
    private static PluginLogger logger;

    @Override
    public void onEnable() {
        logger =(PluginLogger)super.getLogger();
        logger.setTag(new ChatArguments().append(ChatStyle.PURPLE).append("[").append(ChatStyle.GOLD).append("Warriors").append(ChatStyle.PURPLE).append("]").append(ChatStyle.RESET));
        Warriors.getLog().info(" Enabling Warriors...");
        plugin = this;
        mysql = WarHeadUtils.getMysqlManager((CommonPlugin)Warriors.getPlugin(), "Warriors");
        XpManager xpManager = new XpManager();
        mainThread = Thread.currentThread();
        vanillaTriggerListener = new VanillaListener();
        triggerListener = new WarriorsTriggerListener();
        spoutListener = new WarriorsListener();
        wc = new WarriorsCommand(plugin);
        wcs = new WarriorsCommands(plugin);
        cooldown = new CoolDown();
        Warriors.getLog().info(" Checking and creating SQL tables if needed...");
        createTables();
        Warriors.getLog().info(" ...Done");
        Warriors.getLog().info(" Loading XStream...");
        File lib1 = new File("lib/");
        File lib2 = new File("lib/xstream/");
        File lib3 = new File("lib/xstream-hibernate/");
        this.downloadXSplit();
        for(String filename: lib1.list()){
            if(filename.contains(".jar")){
                Warriors.getPlugin().loadLibrary(new File(lib1, filename));
            }
        }
        for(String filename: lib2.list()){
            if(filename.contains(".jar")){
                Warriors.getPlugin().loadLibrary(new File(lib2, filename));
            }
        }
        for(String filename: lib3.list()){
            if(filename.contains(".jar")){
                Warriors.getPlugin().loadLibrary(new File(lib3, filename));
            }
        }
        Warriors.getLog().info("...Done");
        Warriors.getLog().info(" Loading Classes...");
        WarriorsManager.getWarCharacterLoader().loadAllWarCharacters();
        Warriors.getLog().info(" ...Done");
        Warriors.getLog().info(" Loading Races...");
        WarriorsManager.getRaceLoader().loadAllRaces();
        Warriors.getLog().info(" ...Done");
        Warriors.getLog().info(" Loading Skills...");
        WarriorsManager.getSkillLoader().loadAllSkills();
        Warriors.getLog().info(" ...Done");
        Warriors.getLog().info(" Loading Achievements...");
        WarriorsManager.getAchievementLoader().loadAllAchievements();
        Warriors.getLog().info(" ...Done");
        itemHandler = new VanillaItemHandler();
        config = new WarriorsConfig((Configuration)new YamlConfiguration(new File(getDataFolder(), "Warriors.yml")));
        Warriors.getLog().info(" Loading Vanilla and Spout Listeners...");
        
        /*
         * register event listeners
         */
        plugin.getEngine().getEventManager().registerEvents(vanillaTriggerListener, plugin);
        plugin.getEngine().getEventManager().registerEvents(spoutListener, plugin);
        plugin.getEngine().getEventManager().registerEvents(triggerListener, plugin);
        
        /*
         * register command listeners
         */
        CommandRegistrationsFactory<Class<?>> commandRegFactory = new AnnotatedCommandRegistrationFactory(new SimpleInjector(plugin), new SimpleAnnotatedCommandExecutorFactory());
        plugin.getEngine().getRootCommand().addSubCommands(plugin,WarriorsCommand.class, commandRegFactory);
        Warriors.getLog().info(" ...Done");
        Warriors.getLog().info(" Warrors loaded!");
    }

    @Override
    public void onDisable() {

    }

    public static Thread getMainThread(){
        return mainThread;
    }

    public void createTables(){
        /*
         * player table
         */
        mysql.createTable("CREATE TABLE IF NOT EXISTS `warriors_players` "
                + "(`ID` INT(255) NOT NULL AUTO_INCREMENT, `player_name` varchar(50) NOT NULL,"
                + " `character_name` varchar(50), `character_2_name` varchar(50), `race_name` varchar(50), `xp` INT(50),"
                + " `mana` INT(50), `stamina` INT(50), `race_changes` INT(10), PRIMARY KEY(`ID`))");
        /*
         * skill point table
         */
        mysql.createTable("CREATE TABLE IF NOT EXISTS `warriors_skill_points` "
                + "(`ID` INT(255) NOT NULL AUTO_INCREMENT, `player_name` varchar(50) NOT NULL,"
                + " `tool` varchar(1000), `health` INT(10),"
                + " `mana` INT(10), `stamina` INT(10), `tool_damage` INT(10),"
                + " `mana_regen` INT(10), `stamina_regen` INT(10), `health_regen` INT(10),"
                + " `unused` INT(10), PRIMARY KEY(`ID`))");
    }

    public static CommonPlugin getPlugin(){
        return plugin;
    }

    public static MySQLManager getDatabase(){
        return mysql;
    }
    
    public static WarriorsConfig getConfig(){
        return config;
    }
    
    public static ItemHandler getItemHandler(){
        return itemHandler;
    }
    
    public static ChatArguments getChatTag(){
        return new ChatArguments().append(ChatStyle.BLACK).append("[").append(ChatStyle.GOLD).append("Warriors").append(ChatStyle.BLACK).append("] ").append(ChatStyle.WHITE);
    }
    
    public static CoolDown getCooldownManager(){
        return cooldown;
    }
    
    public static PluginLogger getLog(){
        return logger;
    }
    
    private void downloadXSplit(){
        File jdbc = new File("lib/xstream-1.4.4.jar");
        if(!jdbc.exists()){
            try {
                saveUrl("xstream.zip", "https://nexus.codehaus.org/content/repositories/releases/com/thoughtworks/xstream/xstream-distribution/1.4.4/xstream-distribution-1.4.4-bin.zip");
            } catch (MalformedURLException ex) {
                Logger.getLogger(Warriors.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Warriors.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.getZipFiles("xstream.zip", "lib/");
        }
    }
    
    /**
     *
     * @param filename
     * @param urlString
     * @throws MalformedURLException
     * @throws IOException
     */
    public void saveUrl(String filename, String urlString) throws MalformedURLException, IOException
    {
    	BufferedInputStream in = null;
    	FileOutputStream fout = null;
        Spout.getLogger().info("[Warriors] Downloading: " + filename);
    	try
    	{
    		in = new BufferedInputStream(new URL(urlString).openStream());
    		fout = new FileOutputStream(filename);

    		byte data[] = new byte[1024];
    		int count;
    		while ((count = in.read(data, 0, 1024)) != -1)
    		{
    			fout.write(data, 0, count);
    		}
    	}
    	finally
    	{
    		if (in != null)
    			in.close();
    		if (fout != null)
    			fout.close();
    	}
        Spout.getLogger().info("[Warriors] Downloading Complete: " + filename);
    }

    /**
     *
     * @param filename
     */
    public void getZipFiles(String zipfrom, String zipto) {
        Enumeration entriesEnum;
        ZipFile zipFile;
        try {
            zipFile = new ZipFile(zipfrom);
            entriesEnum = zipFile.entries();

            File directory = new File(zipto);
            Spout.getLogger().info("[Warriors] Extracting: " + zipfrom);

            /**
             * Check if the directory to extract to exists
             */
            if (!directory.exists()) {
                directory.mkdirs();
            }
            while (entriesEnum.hasMoreElements()) {
                try {
                    /*
                     * get the zip entry and create the location for it to be copied to
                     */
                    ZipEntry entry = (ZipEntry) entriesEnum.nextElement();
                    String name = entry.getName();
                    name = name.contains("xstream-1.4.4/lib/") ? name.replace("xstream-1.4.4/lib/", "") : name;
                    File dest = new File(zipto + name);
                    /*
                     * if it is a directory just create and skip :D
                     */
                    if(entry.isDirectory()){
                        dest.mkdirs();
                        continue;
                    }
                    /*
                     * make sure we are overwriting any pre-existing files
                     * and lets set it to writable and readable shall we?
                     */
                    dest.delete();
                    dest.getParentFile().mkdirs();
                    dest.setReadable(true);
                    dest.setWritable(true);
                    /*
                     * create our in/out streams
                     */
                    InputStream in = zipFile.getInputStream(entry);
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
                    /*
                     * copy the file
                     */
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = in.read(buffer)) >= 0) {
                        out.write(buffer, 0, len);
                    }
                    /*
                     * always remember to close
                     */
                    in.close();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            zipFile.close();
        } catch (IOException ioe) {
            Spout.getLogger().info("Exception Extracting XStream.jar:");
            ioe.printStackTrace();
            return;
        }
        Spout.getLogger().info("[Warriors] Extraction Complete: " + zipfrom);
        Spout.getLogger().info("[Warriors] The XSplit license has been provided for you"
                + "and can be found in your Main Spout Directory inside the 'lib/xstream-1.4.4/'"
                + " folder.");
    }
}
