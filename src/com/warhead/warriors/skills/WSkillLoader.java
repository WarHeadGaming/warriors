package com.warhead.warriors.skills;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.skill.Skill;
import com.warhead.warriors.api.skill.SkillDescriptionFile;
import com.warhead.warriors.api.skill.SkillLoader;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.Spout;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author somners
 */
public class WSkillLoader implements SkillLoader {

    private static HashMap<String, Skill> skills = new HashMap<String, Skill>();
    private static File skillDir = new File("plugins/Warriors/Skills/");
    
    @Override
    public boolean isSkillLoaded(String skillName){
        return skills.containsKey(skillName);
    }

    @Override
    public List<String> getSkillNames(){
        List<String> skills = new ArrayList();
        String[] skillsArray = (String[])this.skills.keySet().toArray();
        for(int i = 0 ; skillsArray.length < i ; i++){
            skills.add(skillsArray[i]);
        }
        return skills;
    }

    @Override
    public List<Skill> getSkills(){
        List<Skill> skills = new ArrayList();
        Skill[] skillsArray = (Skill[])this.skills.values().toArray();
        for(int i = 0 ; skillsArray.length < i ; i++){
            skills.add(skillsArray[i]);
        }
        return skills;
    }

    @Override
    public Skill getSkill(String skillName){
        return skills.get(skillName);
    }

    @Override
    public void loadSkill(String skillName) {
        String[] list = skillDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(skillName+".jar")){
                loadSkill(new File(skillDir.getPath() + skillName + ".jar"));
            }
        }

    }

    @Override
    public void loadSkill(File file){
        InputStream in = null;
        String main = null;
        String name = null;
        Skill skill = null;
        try {
            ClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()}, Warriors.getPlugin().getClassLoader());
            URL skillDescURL = loader.getResource("Skill.yml");
            
            URLConnection skillDescConn = skillDescURL.openConnection();
            
            if(Spout.debugMode()){
                Warriors.getLog().info(skillDescConn.toString());
            }
            
            in = skillDescConn.getInputStream();
            
            if(Spout.debugMode()){
                Warriors.getLog().info(skillDescConn.getURL().toString());
                Warriors.getLog().info(skillDescConn.getContent().toString());
            }
            
            WSkillDescriptionFile skillDesc = new WSkillDescriptionFile(new YamlConfiguration(in));
            main = skillDesc.getMainClass();
            
            Warriors.getLog().info("Main = " + main);
            name = skillDesc.getName();
            Class<?> skillMain = loader.loadClass(main);
            skill = (Skill) skillMain.newInstance();
            
            
        } catch (InstantiationException ex) {
            Logger.getLogger(WSkillLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WSkillLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WSkillLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Warriors.getLog().severe(" Read error loading skill.");
            Warriors.getLog().severe(ex.getMessage());
        } finally {
            try {
                if(in != null){
                    in.close();
                }
            } catch (IOException ex) {
                Warriors.getLog().severe(" Close error post-loading a skill.");
                Warriors.getLog().severe(ex.getMessage());
            }
        }
        if(name == null){
            Warriors.getLog().severe(" Invalid or no name loading skill with main class '"+main+"'");
            return;
        }
        if(skill == null){
            Warriors.getLog().severe(" Something went wrong loading skill with main class '"+main+"'");
            return;
        }
        skill.setName(name);
        skills.put(name, skill);
        skill.onEnable();
        Warriors.getLog().severe(" Skill '"+name+"' has been loaded.");
    }

    @Override
    public void loadAllSkills() {
        String[] list = skillDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".jar")){
                loadSkill(new File(skillDir, list[i]));
            }
        }
    }

    @Override
    public void unloadSkill(String skillName) {
        if(skills.containsKey(skillName)){
            skills.remove(skillName);
            Warriors.getLog().severe(" Skill '"+skillName+"' has been unloaded.");
            return;
        }
        Warriors.getLog().severe(" Skill '"+skillName+"' is not loaded and cannot be unloaded.");
    }

    @Override
    public void reloadAllSkills() {
        skills.clear();
        loadAllSkills();
    }

    @Override
    public void reloadSkill(String skillName) {
        unloadSkill(skillName);
        loadSkill(skillName);
    }

    @Override
    public SkillDescriptionFile getSkillDescriptionFile(String skillName) {
        String[] list = skillDir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(skillName+".jar")){
                InputStream in = null;
                try {
                    File file = new File(skillDir.getPath() + skillName + ".jar");
                    URLClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()}, Thread.currentThread().getContextClassLoader());
                    URL skillDescURL = loader.getResource("Skill.properties");
                    URLConnection skillDescConn = skillDescURL.openConnection();
                    in = skillDescConn.getInputStream();
                    return new WSkillDescriptionFile(new YamlConfiguration(in));
                } catch (IOException ex) {
                    Logger.getLogger(WSkillLoader.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(WSkillLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        }
        return null;
    }
}
