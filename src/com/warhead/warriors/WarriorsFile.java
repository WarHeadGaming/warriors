package com.warhead.warriors;

import com.warhead.warriors.character.WClassFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.ConfigurationNode;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author Somners
 */
public class WarriorsFile extends ConfigurationHolderConfiguration {
    
    private YamlConfiguration config;
    private ConfigurationHolder className = new ConfigurationHolder(config, (Object)"Default Name", "Core", "Class Name");
    private ConfigurationHolder skills = new ConfigurationHolder(config, (Object)new ArrayList(), "Core", "Skills");
    private ConfigurationHolder oreXP = new ConfigurationHolder(config, (Object)"", "Core", "Xp Rewards", "Ores");
    private ConfigurationHolder mobXP = new ConfigurationHolder(config, (Object)"", "Core", "Xp Rewards", "Mobs");
    private ConfigurationHolder weapons = new ConfigurationHolder(config, (Object)"", "Core", "Weapon");
    private ConfigurationHolder tools = new ConfigurationHolder(config, (Object)"", "Core", "Tool");
    private ConfigurationHolder bannedRaces = new ConfigurationHolder(config, (Object)"", "Core", "Banned Races");

    public WarriorsFile(Configuration config) throws ConfigurationException {
        super(config);
        this.config = (YamlConfiguration) config;
        load();
    }
    
    public Object getValue(String node){
        return config.getNode(node).getValue();
    }
    
    public Map<String, ConfigurationNode> getChildMap(String node){
        return config.getChildren();
    }
    
    public Map<String, Integer> getSubChildMap(Map<String, ConfigurationNode> map, String subNodeName){
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            returnMap.put(key, map.get(key).getNode(subNodeName).getInt());
        }
        return returnMap;
    }
    
    public List<String> getStringList(String node) {
        return config.getNode(node).getStringList();
    }
    
    
    
    
    public boolean doesEntryExist(String node, Object defaultValue) {
        ConfigurationNode testNode = config.getNode(node);
        if (testNode.getValue() == null) {
            try {
                config.createConfigurationNode(node.split("\\."), defaultValue);
                config.save();
            } catch (ConfigurationException ex) {
                Logger.getLogger(WClassFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        return true;
    }
    
    public boolean doesListExist(String node, List defaultValue) {
        ConfigurationNode testNode = config.getNode(node);
        if (testNode.getList().isEmpty()) {
            try {
                config.addChild(new ConfigurationNode(config, node.split("\\."), defaultValue));
                config.save();
            } catch (ConfigurationException ex) {
                Logger.getLogger(WClassFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        return true;
    }
    
    @Override
    public void save() throws ConfigurationException{
        super.save();
    }
    
    @Override
    public void load() throws ConfigurationException {
        super.load();
        super.save();
    }
    
}
