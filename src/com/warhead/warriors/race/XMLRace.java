/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.race;

import com.thoughtworks.xstream.XStream;
import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.character.XMLClass;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class XMLRace implements Race {

    private WarriorsRace warriorsRace;
    private XStream stream;

    public XMLRace(String name){
        
        InputStream in = null;
        try {
            File file = new File("plugins/Warriors/Races/"+name+".xml");
            in = file.toURI().toURL().openStream();
            stream = new XStream();
            stream.alias("entry", String.class);
            stream.alias("race", WarriorsRace.class);
            stream.alias("health", Health.class);
            stream.alias("mana", Mana.class);
            stream.alias("stamina", Stamina.class);
            stream.alias("race", WarriorsRace.class);
            warriorsRace = (WarriorsRace) stream.fromXML(in);
        } catch (IOException ex) {
            Logger.getLogger(XMLClass.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(XMLClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getName() {
        try{
            return warriorsRace.name;
        } catch(NullPointerException ex) {
            Spout.getLogger().severe("Could not get Race Name! Check for defaulted Race Name!");
            Spout.getLogger().severe(ex.getMessage());
            return "";
        }
    }

    @Override
    public int getBaseMana() {
        if(warriorsRace.mana.base != null){
            return warriorsRace.mana.base.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getMaxMana() {
        if(warriorsRace.mana.max != null){
            return warriorsRace.mana.max.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getBaseStamina() {
        if(warriorsRace.stamina.base != null){
            return warriorsRace.stamina.base.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getMaxStamina() {
        if(warriorsRace.stamina.max != null){
            return warriorsRace.stamina.max.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getBaseHealth() {
        if(warriorsRace.health.base != null){
            return warriorsRace.health.base.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getMaxHealth() {
        if(warriorsRace.health.max != null){
            return warriorsRace.health.max.intValue();
        } else {
            return 0;
        }
    }

    @Override
    public int getSPHealthModifier() {
        if(warriorsRace.health.skillPointModifier != null){
            return warriorsRace.health.skillPointModifier.intValue();
        } else {
            return 1;
        }
    }

    @Override
    public int getSPManaModifier() {
        if(warriorsRace.health.skillPointModifier != null){
            return warriorsRace.mana.skillPointModifier.intValue();
        } else {
            return 1;
        }
    }

    @Override
    public int getSPStaminaModifier() {
        if(warriorsRace.stamina.skillPointModifier != null)
            return warriorsRace.stamina.skillPointModifier.intValue();
        else
            return 1;
    }

    @Override
    public int getManaRegenInterval() {
        if(warriorsRace.mana.regenInterval != null)
            return warriorsRace.mana.regenInterval.intValue();
        else
            return 1;
    }

    @Override
    public float getManaRegenRate() {
        if(warriorsRace.mana.regenRate != null)
            return warriorsRace.mana.regenRate.floatValue();
        else
            return .01F;
    }

    @Override
    public int getManaSPRegenModifier() {
        if(warriorsRace.mana.skillPointRegenModifier != null)
            return warriorsRace.mana.skillPointRegenModifier.intValue();
        else
            return 1;
    }

    @Override
    public int getStaminaRegenInterval() {
        if(warriorsRace.stamina.regenInterval != null)
            return warriorsRace.stamina.regenInterval.intValue();
        else
            return 1;
    }

    @Override
    public float getStaminaRegenRate() {
        if(warriorsRace.stamina.regenRate != null)
            return warriorsRace.stamina.regenRate.floatValue();
        else
            return 1;
    }

    @Override
    public int getStaminaSPRegenModifier() {
        if(warriorsRace.stamina.skillPointRegenModifer != null)
            return warriorsRace.stamina.skillPointRegenModifer.intValue();
        else
            return 1;
    }

    @Override
    public int getHealthRegenInterval() {
        if(warriorsRace.health.regenInterval != null)
            return warriorsRace.health.regenInterval.intValue();
        else
            return 1;
    }

    @Override
    public float getHealthRegenRate() {
        if(warriorsRace.health.regenRate != null)
            return warriorsRace.health.regenRate.floatValue();
        else
            return .01F;
    }

    @Override
    public int getHealthSPRegenModifier() {
        if(warriorsRace.health.skillPointRegenModifer != null)
            return warriorsRace.health.skillPointRegenModifer.intValue();
        else
            return 1;
    }

    @Override
    public boolean hasMana() {
	return warriorsRace.mana != null;
    }

    @Override
    public boolean hasStamina() {
	return warriorsRace.stamina != null;
    }
}