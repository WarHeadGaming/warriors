/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.race;

import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.api.races.RaceFile;
import com.warhead.warriors.api.skill.Skill;
import java.util.HashMap;
import org.spout.api.exception.ConfigurationException;

/**
 *
 * @author somners
 */
public class YamlRace implements Race {


    private String name;
    private int baseHealth, maxHealth, baseStamina, maxStamina, baseMana, maxMana;
    private int healthSPMod, manaSPMod, staminaSPMod;
    private int bowDiff, swordDiff;
    private HashMap<String, Integer> skillNames;//stores <Skill Name, Level it can be used at>
    private HashMap<Skill, Integer> skills;//stores <Skill Class, Level it can be used at>
    private HashMap<Integer, Integer> weaponSPDamageMod;//stores <Block ID, Skill Points Needed>
    private RaceFile config;


    public YamlRace(RaceFile config) throws ConfigurationException{
        this.config = config;
        name = this.config.getNameEntry();
        /*
         * lets load our characters skills, shall we?
         */
        skillNames = this.config.getSkillsEntry();

        baseMana = config.getBaseMana();
        maxMana = config.getMaxMana();
        baseHealth = config.getBaseHealth();
        maxHealth = config.getMaxHealth();
        baseStamina = config.getBaseStamina();
        maxStamina = config.getMaxStamina();
        healthSPMod = config.getSPHealthModifier();
        staminaSPMod = config.getSPStaminaModifier();
        manaSPMod = config.getSPManaModifier();
        weaponSPDamageMod = config.getWeaponSPDamageMod();
        bowDiff = config.getBowDamageDiff();
        swordDiff = config.getSwordDamageDiff();


    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getBaseMana() {
        return baseMana;
    }

    @Override
    public int getMaxMana() {
        return maxMana;
    }

    @Override
    public int getBaseStamina() {
        return baseStamina;
    }

    @Override
    public int getMaxStamina() {
        return maxStamina;
    }

    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getMaxHealth() {
        return maxHealth;
    }

    @Override
    public int getSPHealthModifier() {
        return staminaSPMod;
    }

    @Override
    public int getSPManaModifier() {
        return manaSPMod;
    }

    @Override
    public int getSPStaminaModifier() {
        return staminaSPMod;
    }

    @Override
    public int getManaRegenInterval() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public float getManaRegenRate() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getManaSPRegenModifier() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getStaminaRegenInterval() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public float getStaminaRegenRate() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getStaminaSPRegenModifier() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getHealthRegenInterval() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public float getHealthRegenRate() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getHealthSPRegenModifier() {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasMana() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasStamina() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}