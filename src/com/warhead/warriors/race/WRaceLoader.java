/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.race;

import com.warhead.warriors.Warriors;
import com.warhead.warriors.api.races.Race;
import com.warhead.warriors.api.races.RaceLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.spout.api.Spout;

/**
 *
 * @author somners
 */
public class WRaceLoader implements RaceLoader{

    private static HashMap<String, Race> raceMap = new HashMap<String, Race>();
    private static File dir = new File("plugins/Warriors/Races/");

    public WRaceLoader(){
        if(!dir.exists()){
            dir.mkdirs();
            if(Spout.debugMode())
                Warriors.getLog().info(" Race directory created.");
        }
    }

    @Override
    public boolean isRaceLoaded(String raceName){
        return raceMap.containsKey(raceName);
    }

    @Override
    public List<String> getRaceNames(){
        List<String> races = new ArrayList();
        for(String s: raceMap.keySet()){
            races.add(s);
        }
        return races;
    }

    @Override
    public List<Race> getRaces(){
        List<Race> races = new ArrayList();
        Race[] raceArray = (Race[])this.raceMap.values().toArray();
        for(int i = 0 ; raceArray.length < i ; i++){
            races.add(raceArray[i]);
        }
        return races;
    }

    @Override
    public Race getRace(String raceName) {
        return raceMap.get(raceName);
    }

    @Override
    public void loadRace(String name) {
        File xml = new File(dir, name + ".xml");
        File yml = new File(dir, name + ".yml");
        Race race = null;
        if(xml.exists()){
            race = new XMLRace(name);
        }
        else if(yml.exists()){
            
        }
        else{
            Warriors.getLog().severe(" Race'" + name + "' cannot be found. Does it exist?");
            return;
        }
        if(raceMap.containsKey(race.getName())){
            Warriors.getLog().severe(" Class'" + race.getName() + "' is already loaded, why do you have 2 files for it?");
            return;
        }
        raceMap.put(race.getName(), race);
        
        Warriors.getLog().info(" Race '" + race.getName() + "' has been loaded!");

    }

    @Override
    public void loadAllRaces() {
        String[] list = dir.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".xml")){
                loadRace(list[i].replace(".xml", ""));
            }
            else if(list[i].contains(".yml")){
                loadRace(list[i].replace(".yml", ""));
            }
        }
    }

    @Override
    public void unloadRace(String name) {
        if(raceMap.containsKey(name)){
            raceMap.remove(name);
            Warriors.getLog().info(" Race '" + name + "' has been unloaded!");
            return;
        }
        Warriors.getLog().severe(" Race '" + name + "' cannot be unloaded beacause it isn't loaded.");
    }

    @Override
    public File getRaceDir() {
        return dir;
    }
}
