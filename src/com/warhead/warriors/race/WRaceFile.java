/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.race;

import com.warhead.warriors.api.races.RaceFile;
import com.warhead.warriors.character.WClassFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.ConfigurationNode;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author somners
 */
public class WRaceFile extends ConfigurationHolderConfiguration implements RaceFile{

    private YamlConfiguration config;
    private ConfigurationHolder raceName = new ConfigurationHolder(config, (Object)"Default Name", "Race Name");
    private ConfigurationHolder weapons = new ConfigurationHolder(config, (Object)"" , "Weapon Damage Modifiers");
    private ConfigurationHolder healthBase = new ConfigurationHolder(config, (Object)"20" , "Health", "Base");
    private ConfigurationHolder healthMax = new ConfigurationHolder(config, (Object)"40" , "Health", "Max");
    private ConfigurationHolder healthMod = new ConfigurationHolder(config, (Object)"2" , "Health", "Skill Point Modifier");
    private ConfigurationHolder manaBase = new ConfigurationHolder(config, (Object)"50" , "Mana", "Base");
    private ConfigurationHolder manaMax = new ConfigurationHolder(config, (Object)"100" , "Mana", "Max");
    private ConfigurationHolder manaMod = new ConfigurationHolder(config, (Object)"5" , "Mana", "Skill Point Modifier");
    private ConfigurationHolder staminaBase = new ConfigurationHolder(config, (Object)"50" , "Stamina", "Base");
    private ConfigurationHolder staminaMax = new ConfigurationHolder(config, (Object)"100" , "Stamina", "Max");
    private ConfigurationHolder staminaMod = new ConfigurationHolder(config, (Object)"5" , "Stamina", "Skill Point Modifier");

    public WRaceFile(Configuration config) throws ConfigurationException{
        super(config);
        this.config = (YamlConfiguration)config;
        checkChildren();
    }
    
    public void checkChildren() throws ConfigurationException{
        doesEntryExist(weapons, "Weapon Damage Modifiers.Wooden Sword", 0);
        doesEntryExist(weapons, "Weapon Damage Modifiers.Stone Sword", 0);
        doesEntryExist(weapons, "Weapon Damage Modifiers.Iron Sword", 0);
        doesEntryExist(weapons, "Weapon Damage Modifiers.Gold Sword", 0);
        doesEntryExist(weapons, "Weapon Damage Modifiers.Diamond Sword", 0);
        doesEntryExist(weapons, "Weapon Damage Modifiers.Bow", 0);
        save();
    }

    @Override
    public String getNameEntry() {
        return raceName.getString();
    }

    @Override
    public HashMap<String, Integer> getSkillsEntry() {
        Map<String, ConfigurationNode> map = config.getNode("Core.Skills").getChildren();
        HashMap<String, Integer> returnMap = new HashMap<String, Integer>();
        Iterator<String> keys = map.keySet().iterator();
        while(keys.hasNext()){
           String key = keys.next();
           returnMap.put(key, map.get(key).getInt());
        }
        return returnMap;
    }

    @Override
    public List getCustomListEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCustomStringEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCustomIntEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getCustomBooleanEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getCustomDoubleEntry(String node) {
        //TODO: FINISH
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean doesEntryExist(ConfigurationHolder holder, String node, Object defaultValue) {
        ConfigurationNode testNode  = config.getNode(node);
        if(testNode.getValue() == null){
            try {
                config.createConfigurationNode(node.split("\\."), defaultValue);
                save();
            } catch (ConfigurationException ex) {
                Logger.getLogger(WClassFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }
        return true;
    }

    @Override
    public int getBaseHealth() {
        return healthBase.getInt();
    }

    @Override
    public int getBaseMana() {
        return manaBase.getInt();
    }

    @Override
    public int getBaseStamina() {
        return staminaBase.getInt();
    }

    @Override
    public int getMaxHealth() {
        return healthMax.getInt();
    }

    @Override
    public int getMaxMana() {
        return manaMax.getInt();
    }

    @Override
    public int getMaxStamina() {
        return staminaMax.getInt();
    }

    @Override
    public int getSPHealthModifier() {
        return healthMod.getInt();
    }

    @Override
    public int getSPManaModifier() {
        return manaMod.getInt();
    }

    @Override
    public int getSPStaminaModifier() {
        return staminaMod.getInt();
    }

    @Override
    public HashMap<Integer, Integer> getWeaponSPDamageMod() {
        HashMap<Integer, Integer> returnMap = new HashMap<Integer, Integer>();
        Map<String, ConfigurationNode> map = weapons.getChildren();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
//            returnMap.put(VanillaWeapon.getId(key), map.get(key).getNode("Skill Point Damage Modifier").getInt());
        }
        return returnMap;
    }

    @Override
    public int getSwordDamageDiff(){
        Map<String, ConfigurationNode> map = config.getNode("Core.Skill Point Requirements.Weapon").getChildren();
        Iterator<String> keys = map.keySet().iterator();
        int toReturn = 0;
        while(keys.hasNext()){
            String key = keys.next();
            int temp = !key.equalsIgnoreCase("Bow") ? map.get(key).getNode("Max Damage").getInt() - map.get(key).getNode("Base Damage").getInt() : 0;
            if(temp > toReturn){
                toReturn = temp;
            }
        }
        return toReturn;
    }

    @Override
    public int getBowDamageDiff(){
        return config.getNode("Core.Skill Point Requirements.Weapon.Bow.Max Damage").getInt() - config.getNode("Core.Skill Point Requirements.Weapon.Bow.Base Damage").getInt();
    }
    
    @Override
    public void save() throws ConfigurationException{
        super.save();
    }
    
    @Override
    public void load() throws ConfigurationException {
        super.load();
        super.save();
    }
}
