/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.race;

/**
 *
 * @author somners
 */
public class Mana {
    Integer base;
    Integer max;
    Integer skillPointModifier;
    Integer regenInterval;
    Float regenRate;// = .01F;
    Integer skillPointRegenModifier;
}
