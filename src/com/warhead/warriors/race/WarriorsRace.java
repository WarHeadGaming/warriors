/*
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. Use of our code without express consent from the
 * developers is in violation of our license which has been provided
 * for you in the source and compiled jar.
 */
package com.warhead.warriors.race;

import java.util.ArrayList;

/**
 *
 * @author somners
 */
public class WarriorsRace {
    
    public String name;
    public Health health = null;
    public Mana mana = null;
    public Stamina stamina = null;
    public ArrayList<String> bannedClasses = new ArrayList<String>();
    
}
