
package com.warhead.warriors;

import com.warhead.warheadutils.WarHeadUtils;
import com.warhead.warheadutils.database.MySQLManager;
import java.lang.Math;
import org.spout.api.plugin.CommonPlugin;

/**
 *
 * @author somners
 */
public class XpManager {
    

    private static MySQLManager mysql = Warriors.getDatabase();

    public static void addXp(int toAdd, String playerName){
        mysql.update("UPDATE `warriors_players` SET `xp` = '"+ (getXp(playerName) + toAdd) +"' WHERE `player_name` = '"+ playerName +"' LIMIT 1");
    }

    public static void removeXp(int toRemove, String playerName){
        mysql.update("UPDATE `warriors_players` SET `xp` = '"+ (getXp(playerName) - toRemove) +"' WHERE `player_name` = '"+ playerName +"' LIMIT 1");
    }

    public static int getXp(String playerName){
        return mysql.getIntValue("SELECT `xp` FROM `warriors_players` WHERE `player_name` = '"+ playerName +"'", "xp");
    }

    public static void setXp(int toSet, String playerName){
        mysql.update("UPDATE `warriors_players` SET `xp` = '"+ toSet +"' WHERE `player_name` = '"+ playerName +"' LIMIT 1");
    }

    public static int getLevel(int exp){
        return (int) Math.sqrt(exp)/5;
    }

}
