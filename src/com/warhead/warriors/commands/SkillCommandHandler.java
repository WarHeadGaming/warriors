/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warriors.commands;

import com.warhead.warheadutils.util.TwoColumnList;
import com.warhead.warriors.api.components.RPGComponent;
import com.warhead.warriors.api.skill.command.SkillCommand;
import java.util.ArrayList;
import java.util.HashMap;
import org.spout.api.chat.ChatArguments;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.entity.Player;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class SkillCommandHandler {
    
    private static final HashMap<String, SkillCommand> skillCommands = new HashMap<String, SkillCommand>();
    
    @Command(aliases = {"skill", "s"}, usage = "/skill help", desc = "see skill commands you can use", min = 0, max = 20)
    public void executeCommand(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        if(cc.getString(0).equalsIgnoreCase("help") || cc.getString(0).equalsIgnoreCase("?")){
            ArrayList<String[]> temp = new ArrayList<String[]>();
            TwoColumnList tcl = new TwoColumnList();
            int index = 0;
            if(cc.isInteger(1)){
                index = cc.getInteger(1);
            }
            for(SkillCommand sc : skillCommands.values()){
                if(player.get(RPGComponent.class).canPrimaryUseSkill(sc.getSkill().getname()) || player.get(RPGComponent.class).canSecondaryUseSkill(sc.getSkill().getname())){
                    String[] args = sc.getHelpMessage();
                    tcl.append(new ChatArguments().append(args[0]), new ChatArguments().append(args[1]));
                }
            }
            tcl.sendMessage(player, (index - 1)*5, index*5);
            
        }
        else if(skillCommands.containsKey(cc.getString(0))){
            skillCommands.get(cc.getString(0)).execute(cc, (Player)cs);
        }
        
    }
    
    public void registerCommand(String subCommand, SkillCommand commandListener){
        if(!skillCommands.containsKey(subCommand)){
            skillCommands.put(subCommand, commandListener);
        }
    }
        
    public void unRegisterCommand(String subCommand){
        if(skillCommands.containsKey(subCommand)){
            skillCommands.remove(subCommand);
        }
    }
}
