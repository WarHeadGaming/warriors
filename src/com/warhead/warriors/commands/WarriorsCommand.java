package com.warhead.warriors.commands;

import com.warhead.warriors.Warriors;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.NestedCommand;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class WarriorsCommand {
    
    private Warriors plugin;
    
    public WarriorsCommand(Warriors plugin){
        this.plugin = plugin;
    }
    
    @Command(aliases = {"warriors","warrior","w"}, usage = "/w", desc = "Access Warriors commands", min=1, max = 20)
    @NestedCommand(value = WarriorsCommands.class, ignoreBody = true)
    public void WarriorsCommand(CommandContext cc, CommandSource cs) throws CommandException{
        
    }
    
//    @Command(aliases = {"bind","b"}, usage = "/b \\[skillname\\]", desc = "Bind Skills to the item in your hand", min=1, max = 20)
//    public void bind(CommandContext cc, CommandSource cs) throws CommandException{
//        Player player = (Player) cs;
//        int itemId = VanillaMaterials.getMinecraftId(player.get(PlayerInventory.class).getQuickbar().getSelectedSlot().get().getMaterial());
//        if(!WarriorsManager.getSkillLoader().isSkillLoaded(cc.getString(0)) || player.get(RPGComponent.class).canUseSkill(cc.getString(0))){
//            player.sendMessage(new ChatArguments().append(ChatStyle.GOLD).append("You either cannot use or the skill does not exist: ").append(ChatStyle.BLUE).append(cc.getString(0)));
//            return;
//        }
//        
//    }
//    
//    @Command(aliases = {"unbind","ub"}, usage = "/ub <skillname>", desc = "UnBind Skills to the item in your hand", min=1, max = 20)
//    public void unBind(CommandContext cc, CommandSource cs) throws CommandException{
//        Player player = (Player) cs;
//        int itemId = VanillaMaterials.getMinecraftId(player.get(PlayerInventory.class).getQuickbar().getSelectedSlot().get().getMaterial());
//        if(!WarriorsManager.getSkillLoader().isSkillLoaded(cc.getString(0))){
//            player.sendMessage(new ChatArguments().append(ChatStyle.GOLD).append("That skill does not exist: ").append(ChatStyle.BLUE).append(cc.getString(0)));
//            return;
//        }
//        
//        
//    }
}