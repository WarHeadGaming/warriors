package com.warhead.warriors.commands;

import com.warhead.warheadutils.util.OneColumnList;
import com.warhead.warheadutils.util.TwoColumnList;
import com.warhead.warriors.Warriors;
import com.warhead.warriors.WarriorsConfig;
import com.warhead.warriors.api.SkillBindManager;
import com.warhead.warriors.api.Util.SkillPointType;
import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.components.ManaComponent;
import com.warhead.warriors.api.components.RPGComponent;
import com.warhead.warriors.api.components.SkillPointComponent;
import com.warhead.warriors.api.components.StaminaComponent;
import com.warhead.warriors.api.components.ToolComponent;
import com.warhead.warriors.api.components.XpComponent;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.spout.api.Spout;
import org.spout.api.chat.ChatArguments;
import org.spout.api.chat.style.ChatStyle;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.entity.Player;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class WarriorsCommands {
    
    private Warriors plugin;
    private HashMap<String, String> classChoose = new HashMap<String, String>();
    private HashMap<String, String> raceChoose = new HashMap<String, String>();
    private HashMap<String, HashMap<String, Object>> spApply = new HashMap<String, HashMap<String, Object>>();
    private SkillBindManager sbm;
    
    public WarriorsCommands(Warriors plugin){
        this.plugin = plugin;
        sbm = new SkillBindManager();
    }
    
    
    @Command(aliases = {"create", "choose", "c"}, usage = "/w choose {Warriors class}", desc = "Choose a warriors class", min = 1, max = 20)
    public void createCharacter(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player) cs;
        for(int i = 0;i<cc.length(); i++){
            if(cc.get(i).getPlainString().equalsIgnoreCase("--class") || cc.get(i).getPlainString().equalsIgnoreCase("--c")){
                i++;
                StringBuilder sb = new StringBuilder();
                while(i < cc.length() && !cc.get(i).getPlainString().contains("--")){
                    sb.append(cc.get(i).getPlainString()).append(" ");
                    i++;
                }
                if(classChoose.containsKey(player.getName())){
                    classChoose.remove(player.getName());
                }
                classChoose.put(player.getName(), sb.toString().trim());
                i--;
                continue;
            }
            
            if(cc.get(i).getPlainString().equalsIgnoreCase("--race") || cc.get(i).getPlainString().equalsIgnoreCase("--r")){
                i++;
                StringBuilder sb = new StringBuilder();
                while(i < cc.length() && !cc.get(i).getPlainString().contains("--")){
                    sb.append(cc.get(i).getPlainString()).append(" ");
                    i++;
                }
                if(raceChoose.containsKey(player.getName())){
                    raceChoose.remove(player.getName());
                }
                raceChoose.put(player.getName(), sb.toString().trim());
                i--;
                continue;
            } 
            i++;
        }
        TwoColumnList tcl = new TwoColumnList();
        tcl.setHeader(new ChatArguments().append(ChatStyle.GOLD).append("Your currently selected Warrior"), new ChatArguments().append(ChatStyle.BLACK).append("=========================================="));
        tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Class:"), new ChatArguments().append(ChatStyle.GOLD).append(classChoose.containsKey(player.getName())?classChoose.get(player.getName()):"None Selected"));
        tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Race:"), new ChatArguments().append(ChatStyle.GOLD).append(raceChoose.containsKey(player.getName())?raceChoose.get(player.getName()):"None Selected"));
        if(!raceChoose.containsKey(player.getName()) || !classChoose.containsKey(player.getName()) || !WarriorsManager.getWarCharacterLoader().isWarCharacterLoaded(classChoose.get(player.getName())) || !WarriorsManager.getRaceLoader().isRaceLoaded(raceChoose.get(player.getName()))){
            tcl.setFooter(new ChatArguments().append(ChatStyle.RED).append("Your current selection is invalid make sure you have selected a valid Class and Race."));
            tcl.sendMessage(player);
            return;
        }
        if(player.get(RPGComponent.class) != null){
            tcl.setFooter(new ChatArguments().append(ChatStyle.RED).append("Your current selection will reset your character, race, and xp.  If you are sure you want to continue use command: ").append(ChatStyle.DARK_BLUE).append("/w accept"));
            tcl.sendMessage(player);
            return;
        }
        tcl.setFooter(new ChatArguments().append(ChatStyle.RED).append("Use Command: ").append(ChatStyle.DARK_BLUE).append("/w accept").append(ChatStyle.RED).append(" to accept this Character."));
        tcl.sendMessage(player);
    }
    
    
    @Command(aliases = {"accept", "a"}, usage = "/w accept", desc = "Accept Choosen Class and Race", min = 0, max = 0)
    public void acceptCharacter(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        OneColumnList ocl = new OneColumnList();
        if(!classChoose.containsKey(player.getName())){
            ocl.setHeader(new ChatArguments().append("You have not selected a class yet."));
            ocl.append(new ChatArguments().append(ChatStyle.BLUE).append("Proper Usage: ").append(ChatStyle.WHITE).append("/w create --class <class> --race <race>"));
            ocl.sendMessage(player);
            return;
        }
        if(!raceChoose.containsKey(player.getName())){
            ocl.setHeader(new ChatArguments().append("You have not selected a race yet."));
            ocl.append(new ChatArguments().append(ChatStyle.BLUE).append("Proper Usage: ").append(ChatStyle.WHITE).append("/w create --class <class> --race <race>"));
            ocl.sendMessage(player);
            return;
        }
        if(!WarriorsManager.getWarCharacterLoader().isWarCharacterLoaded(classChoose.get(player.getName())) || !WarriorsManager.getRaceLoader().isRaceLoaded(raceChoose.get(player.getName()))){
            ocl.setHeader(new ChatArguments().append("You have not selected a valid race or class."));
            ocl.append(new ChatArguments().append(ChatStyle.BLUE).append("Proper Usage: ").append(ChatStyle.WHITE).append("/w create --class <class> --race <race>"));
            ocl.sendMessage(player);
            return;
        }
        if(!WarriorsManager.getWarCharacterLoader().getWarCharacter(classChoose.get(player.getName())).canUseRace(raceChoose.get(player.getName()))){
            ocl.setHeader(new ChatArguments().append("The race you have selected is not valid for this Character class"));
            ocl.append(new ChatArguments().append(ChatStyle.BLUE).append("Proper Usage: ").append(ChatStyle.WHITE).append("/w create --class <class> --race <race>"));
            ocl.setFooter(new ChatArguments().append(ChatStyle.RED).append("Please select a new class or a valid race for this class."));
            ocl.sendMessage(player);
            return;
        }
        if(player.get(RPGComponent.class) != null){
            player.detach(RPGComponent.class);
        }
        WarriorsManager.createNewWarrior(player.getName(), classChoose.get(player.getName()), raceChoose.get(player.getName()));
        player.add(RPGComponent.class).refreshComponent();
        ocl.setHeader(new ChatArguments().append("You have selected a valid race and class."));
        ocl.append(new ChatArguments().append(ChatStyle.BLACK).append("Class:"), new ChatArguments().append(ChatStyle.GOLD).append(classChoose.get(player.getName())));
        ocl.append(new ChatArguments().append(ChatStyle.BLACK).append("Race:"), new ChatArguments().append(ChatStyle.GOLD).append(classChoose.get(player.getName())));
        ocl.sendMessage(player);
    }
    
    @Command(aliases = {"skillpoint", "sp"}, usage = "/w sp {apply|unused|view} {--category <type>} {amount}", desc = "Choose a warriors class", min = 1, max = 20)
    public void skillPoints(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player) cs;
        RPGComponent rpg = player.get(RPGComponent.class);
        /*
         * Check unused skill points
         */
        if(cc.getString(0).equalsIgnoreCase("unused")){
            OneColumnList ocl = new OneColumnList();
            ocl.setHeader(new ChatArguments().append("Your remaining unused Skill Points are: ").append(ChatStyle.GOLD).append(player.get(SkillPointComponent.class).getSkillPoints()));
            ocl.sendMessage(player);
            return;
        }
        /*
         * View applied skill points
         */
        if(cc.getString(0).equalsIgnoreCase("view")){
            TwoColumnList tcl = new TwoColumnList();
            tcl.setHeader(new ChatArguments().append("Your Skill Point Information: ").append(ChatStyle.GOLD).append(player.get(SkillPointComponent.class).getSkillPoints()));
            tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Unused:"), new ChatArguments().append(ChatStyle.GOLD).append(player.get(SkillPointComponent.class).getSkillPoints()));
            tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Tool Damage:"), new ChatArguments().append(ChatStyle.GOLD).append(player.get(ToolComponent.class).getDamageSP()));
            tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Mana Points:"), new ChatArguments().append(ChatStyle.GOLD).append(player.get(ManaComponent.class).getManaPoints()));
            tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Stamina Points:"), new ChatArguments().append(ChatStyle.GOLD).append(player.get(StaminaComponent.class).getStaminaPoints()));
            tcl.append(new ChatArguments().append(ChatStyle.BLACK).append("Health Points:"), new ChatArguments().append(ChatStyle.GOLD).append(rpg.getHealth().getHealthPoints()));
            tcl.sendMessage(player);
            return;
        }
        /*
         * Apply skill points
         */
        
        if(cc.getString(0).equalsIgnoreCase("apply")){

            if (spApply.containsKey(player.getName())){ 
                spApply.remove(player.getName());
            }
            for(int i = 0;i<cc.length();){

                if(cc.get(i).getPlainString().equalsIgnoreCase("--category") || cc.get(i).getPlainString().equalsIgnoreCase("--c") ){
                    i++;
                    StringBuilder sb = new StringBuilder();
                    while(i < cc.length() && !cc.get(i).getPlainString().contains("--")){
                        sb.append(cc.get(i).getPlainString()).append(" ");
                        i++;
                    }
                    if(spApply.containsKey(player.getName())){
                        spApply.get(player.getName()).put("category", sb.toString().trim());
                    }
                    else {
                        HashMap<String, Object> tempMap = new HashMap<String, Object>();
                        tempMap.put("category", sb.toString().trim());
                        spApply.put(player.getName(), tempMap);
                    }
                    i--;
                    continue;
                }

                if(cc.get(i).getPlainString().equalsIgnoreCase("--amount") || cc.get(i).getPlainString().equalsIgnoreCase("--a")){
                    i++;
                    int amount = 0;
                    while(i<cc.length() && !cc.get(i).getPlainString().contains("--")){
                        amount = cc.getInteger(i);
                        i++;
                    }
                    if(spApply.containsKey(player.getName())){
                        spApply.get(player.getName()).put("amount", amount);
                    }
                    else {
                        HashMap<String, Object> tempMap = new HashMap<String, Object>();
                        tempMap.put("amount", amount);
                        spApply.put(player.getName(), tempMap);
                    }
                    i--;
                    continue;
                }

                i++;

            }
            /*
             * Now lets make sure their selection was valid
             */
            TwoColumnList tcl = new TwoColumnList();
            if(!spApply.containsKey(player.getName())){
                tcl.setHeader(new ChatArguments().append("You have done something Horribly wrong here. Try Again!"));
                tcl.sendMessage(player);
                return;
            }
            if(!spApply.get(player.getName()).containsKey("category") || !SkillPointType.exists((String)spApply.get(player.getName()).get("category"))){
                tcl.setHeader(new ChatArguments().append("You have not selected a valid category. Try Again!"));
                tcl.sendMessage(player);
                return;
            }
            int amount = spApply.get(player.getName()).containsKey("amount") ? (Integer) (spApply.get(player.getName()).get("amount")) : 1;
            String category = (String)spApply.get(player.getName()).get("category");
            Warriors.getLog().info(category +"   "+amount);
            if(player.get(SkillPointComponent.class).getSkillPoints() < amount){
                tcl.setHeader(new ChatArguments().append("You don't have that many unused Skill Points. Try Again!"));
                tcl.sendMessage(player);
                return;
            }

            if (SkillPointType.ITEM_DAMAGE.equalsName(category)){
                if(player.get(ToolComponent.class).isDamageSPMaxed()){
                    tcl.setHeader(new ChatArguments().append("You have maxed out your Sword Damage Points. Try Again!"));
                    tcl.sendMessage(player);
                    return;
                }
                else{
                    rpg.getSkillPointComponent().removeSkillPoints(amount);
                    rpg.getToolComponent().addDamageSP(amount);
                    tcl.setHeader(new ChatArguments().append("You have added Skill Points to Tool Damage."));
                    tcl.append(new ChatArguments().append(ChatStyle.BLUE).append("Tool Damage Points:"), new ChatArguments().append(ChatStyle.GOLD).append(amount));
                    tcl.sendMessage(player);
                    return;
                }
            }
            if (SkillPointType.HEALTH.equalsName(category) ) {
                if ( rpg.getHealth().isHealthMaxed()){
                    tcl.setHeader(new ChatArguments().append("You have maxed out your Health Points. Try Again!"));
                    tcl.sendMessage(player);
                    return;
                }
                else{
                    rpg.getSkillPointComponent().removeSkillPoints(amount);
                    rpg.getHealth().setHealthPoint(rpg.getHealth().getHealthPoints() + amount);
                    tcl.setHeader(new ChatArguments().append("You have added Skill Points to Health."));
                    tcl.append(new ChatArguments().append(ChatStyle.BLUE).append("Health Points:"), new ChatArguments().append(ChatStyle.GOLD).append(amount));
                    tcl.sendMessage(player);
                    return;
                }
            }
            if (SkillPointType.MANA.equalsName(category) ) {
                if ( player.get(ManaComponent.class).isManaMaxedOut()){
                    tcl.setHeader(new ChatArguments().append("You have maxed out your Mana Points. Try Again!"));
                    tcl.sendMessage(player);
                    return;
                }
                else{
                    rpg.getSkillPointComponent().removeSkillPoints(amount);
                    rpg.getManaComponent().addManaPoints(amount);
                    tcl.setHeader(new ChatArguments().append("You have added Skill Points to Mana."));
                    tcl.append(new ChatArguments().append(ChatStyle.BLUE).append("Mana Points:"), new ChatArguments().append(ChatStyle.GOLD).append(amount));
                    tcl.sendMessage(player);
                    return;
                }
            }
            if (SkillPointType.STAMINA.equalsName(category) ) {
                if ( player.get(StaminaComponent.class).isStaminaMaxedOut()){
                    tcl.setHeader(new ChatArguments().append("You have maxed out your Stamina Points. Try Again!"));
                    tcl.sendMessage(player);
                    return;
                }
                else{
                    rpg.getSkillPointComponent().removeSkillPoints(amount);
                    rpg.getStaminaComponent().addStaminaPoints(amount);
                    tcl.setHeader(new ChatArguments().append("You have added Skill Points to Stamina."));
                    tcl.append(new ChatArguments().append(ChatStyle.BLUE).append("Stamina Points:"), new ChatArguments().append(ChatStyle.GOLD).append(amount));
                    tcl.sendMessage(player);
                    return;
                }
            }
            if (SkillPointType.HEALTH_REGEN.equalsName(category) ) {
                if ( player.get(StaminaComponent.class).isStaminaMaxedOut()){
                    tcl.setHeader(new ChatArguments().append("You have maxed out your Stamina Points. Try Again!"));
                    tcl.sendMessage(player);
                }
                else{
                    rpg.getSkillPointComponent().removeSkillPoints(amount);
                    rpg.getStaminaComponent().addStaminaPoints(amount);
                    tcl.setHeader(new ChatArguments().append("You have added Skill Points to Stamina."));
                    tcl.append(new ChatArguments().append(ChatStyle.BLUE).append("Stamina Points:"), new ChatArguments().append(ChatStyle.GOLD).append(amount));
                    tcl.sendMessage(player);
                }
            }
        }
    }
    
    @Command(aliases = {"list"}, usage = "/w list {class|race}", desc = "list the classes/races", min = 1, max = 20)
    public void listStuff(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player) cs;
        OneColumnList ocl = new OneColumnList();
        /*
         * check to make sure they entered something to list
         */
        if(cc.length() == 0 || (!cc.getString(0).equalsIgnoreCase("class") && !cc.getString(0).equalsIgnoreCase("race"))){
            ocl.setHeader(new ChatArguments().append("Please select one of the following:"));
            ocl.append(new ChatArguments().append("/w list class <Page #>"));
            ocl.append(new ChatArguments().append("/w list race <Page #>"));
            return;
        }
        
        if(cc.getString(1).equalsIgnoreCase("class")){
            int page = 1;
            if(cc.length() == 2 && cc.isInteger(1)){
                page = cc.getInteger(1);
            }
            List classes = WarriorsManager.getWarCharacterLoader().getWarCharacterNames();
            ocl.setHeader(new ChatArguments().append("Available Classes:"));
            for(int i = page * 5 ; i < classes.size() && i < ((page*5)+5); i++ ){
                ocl.append(new ChatArguments().append(classes.get(i)));
            }
            ocl.setFooter(new ChatArguments().append(ChatStyle.DARK_GRAY).append("For More:  /w list class <Page #>"));
            return;
        }
        
        if(cc.getString(1).equalsIgnoreCase("race")){
            int page = 1;
            if(cc.length() == 2 && cc.isInteger(1)){
                page = cc.getInteger(1);
            }
            List classes = WarriorsManager.getRaceLoader().getRaceNames();
            ocl.setHeader(new ChatArguments().append("Available Races:"));
            for(int i = page * 5 ; i < classes.size() && i < ((page*5)+5); i++ ){
                ocl.append(new ChatArguments().append(classes.get(i)));
            }
            ocl.setFooter(new ChatArguments().append(ChatStyle.DARK_GRAY).append("For More:  /w list class <Page #>"));
        }

    }
        
    @Command(aliases = {"class"}, usage = "/w list {class|race}", desc = "list the classes/races", min = 0, max = 20)
    public void listSkills(CommandContext cc, CommandSource cs) throws CommandException{
        
        
        
    }
    
    @Command(aliases = {"bind"}, usage = "/w bind {skill}", desc = "", min = 1, max = 20)
    public void bindSkills(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = null;
        if(cs instanceof Player){
            player = (Player)cs;
            if(player.get(RPGComponent.class) == null){
                return;
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < cc.length() ; i++){
            sb.append(cc.getString(i)).append(" ");
        }
        String skill = sb.toString().trim();

        if(Spout.debugMode()){
            Spout.getLogger().info(player.getName());
            Spout.getLogger().info(skill);
        }
        
        if(!WarriorsManager.getSkillLoader().isSkillLoaded(skill)){
            player.sendMessage(WarriorsConfig.getServerHeader().append("Skill, " + skill + ", does not appear to exist."));
            return;
        }
        
        if(!player.get(RPGComponent.class).canPrimaryUseSkill(skill) && !player.get(RPGComponent.class).canSecondaryUseSkill(skill)){
            player.sendMessage(WarriorsConfig.getServerHeader().append(" You are not allowed to use skill, " + skill + "."));
            return;
        }
        
        if(sbm.isBound(player, skill)){
            player.sendMessage(WarriorsConfig.getServerHeader().append(" You have already bound skill, " + skill + ", to item."));
        } else {
            if(sbm.bindSkill(player, skill)){
                player.sendMessage(WarriorsConfig.getServerHeader().append(" You have bound skill, " + skill + ", to item."));
            } else {
                player.sendMessage(WarriorsConfig.getServerHeader().append(" Error binding skill, " + skill + ", to item."));
            }
        }
    }
    
    @Command(aliases = {"unbind"}, usage = "/w unbind {skill}", desc = "", min = 0, max = 20)
    public void unbindSkills(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = null;
        if(cs instanceof Player){
            player = (Player)cs;
            if(player.get(RPGComponent.class) == null){
                return;
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < cc.length() ; i++){
            sb.append(cc.getString(i)).append(" ");
        }
        String skill = sb.toString().trim();

        if(sbm.isBound(player ,skill)){
            if(sbm.unBindSkill(player, skill)){
                player.sendMessage(WarriorsConfig.getServerHeader().append(" You have unbound skill, " + skill + ", to item."));
            }
            else{
                player.sendMessage(WarriorsConfig.getServerHeader().append(" Error unbinding skill, " + skill + ", to item."));
            }
//            return;
        }
        else{
            player.sendMessage(WarriorsConfig.getServerHeader().append(" You have not bound skill, " + skill + ", to item."));
        }
        
        
    }
        
    @Command(aliases = {"test"}, usage = "/w list {class|race}", desc = "list the classes/races", min = 0, max = 20)
    public void test(CommandContext cc, CommandSource cs) throws CommandException{
        for(String s: WarriorsManager.getRaceLoader().getRaceNames()){
            Warriors.getLog().info("|||TEST|||   "+s);
        }
        for(String s: WarriorsManager.getWarCharacterLoader().getWarCharacterNames()){
            Warriors.getLog().info("|||TEST|||   "+s);
        }
        Warriors.getLog().info("|||TEST|||   "+ ((Player)cs).get(RPGComponent.class).getPrimaryClass().getName());
        Warriors.getLog().info("|||TEST|||   "+ ((Player)cs).get(RPGComponent.class).getPrimaryClass().getSkillLevel("Thunder"));
        Warriors.getLog().info("|||TEST|||   "+ ((Player)cs).get(XpComponent.class).getXp());
        Warriors.getLog().info("|||TEST|||   "+ ((Player)cs).get(XpComponent.class).getLevel());
        
        
    }    
    
    
}
