package com.warhead.warriors;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.chat.ChatArguments;
import org.spout.api.chat.style.ChatStyle;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;

/**
 *
 * @author somners
 */
public class WarriorsConfig extends ConfigurationHolderConfiguration {
    
    private static Configuration config;
    private static ConfigurationHolder serverName = new ConfigurationHolder(config, (Object)"Server", "Server Name");
    
    public WarriorsConfig(Configuration config){
        super(config);
        this.config = config;
        load();
        
    }
    
    public static String getServerName(){
        return serverName.getString();
    }
    
    public static ChatArguments getServerHeader(){
        return new ChatArguments().append(ChatStyle.BLACK).append("[").append(ChatStyle.GOLD).append(getServerName()).append(ChatStyle.BLACK).append("]").append(ChatStyle.WHITE);
    }
    
    public void load(){
        try {
            super.load();
            super.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(WarriorsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void save(){
        try {
            super.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(WarriorsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
